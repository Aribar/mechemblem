﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathMap
{
    public Dictionary<BattleMapTile, BattleMapTile> previousBattleMapTile = new Dictionary<BattleMapTile, BattleMapTile>();
    public Dictionary<BattleMapTile, int> costSoFar = new Dictionary<BattleMapTile, int>();
    public BattleMapTile startTile;
    public BattleMapTile goalTile;

    public PathMap(BattleMapTile startTile, BattleMapTile goalTile)
    {
        previousBattleMapTile.Add(startTile, null);
        costSoFar.Add(startTile, 0);
        this.startTile = startTile;
        this.goalTile = goalTile;
    }

    /// <summary>
    /// If previousBattleMapTile contains goalTile, it has a valid route!
    /// </summary>
    /// <returns></returns>
    public bool HasValidRoute()
    {
        if (previousBattleMapTile.ContainsKey(goalTile))
            return true;
        return false;
    }

    #region ToString Methods
    /// <summary>
    /// Returns the ENTIRE PathMap.
    /// Format: Path([Start]-[Goal]): T[Tile]([Previous Tile]/[Cost So Far]),
    /// </summary>
    /// <returns></returns>
    public string ToStringPath()
    {
        string result = "";
        if (goalTile == null)
            result = string.Format("Path ({0}-n/a): ", startTile.ToString());
        else if (previousBattleMapTile.ContainsKey(goalTile))
        {
            result = string.Format("Path ({0}-{1} REACHES GOAL): ", startTile.ToString(), goalTile.ToString());
        }
        else
            result = string.Format("Path ({0}-{1} doesn't reach goal): ", startTile.ToString(), goalTile.ToString());

        foreach (BattleMapTile tile in previousBattleMapTile.Keys)
        {
            previousBattleMapTile.TryGetValue(tile, out BattleMapTile previousTile);
            costSoFar.TryGetValue(tile, out int cost);
            result += string.Format("<b>Current{0}</b>(Previous{1}/{2}), ", tile.ToString(), previousTile, cost);
        }
        return result;
    }

    /// <summary>
    /// Returns the route!
    /// Format: Route([Start]-[Goal]): T[Tile]([Previous Tile]/[Cost So Far]),
    /// </summary>
    /// <param name="displayStartToGoal">Determines whether the result is Start-to-Goal or Goal-to-Start.</param>
    /// <returns></returns>
    public string ToStringRoute(bool displayStartToGoal)
    {
        if(goalTile == null)
        {
            return "ToStringRoute, CANNOT DISPLAY ROUTE; NO GOAL!";
        }
        string result = string.Format("Route ({0}-{1}): ", startTile.ToString(), goalTile.ToString());
        List<BattleMapTile> route = GetRouteFromPathMap(displayStartToGoal);

        if (route == null)
        {
            result += "No route!";
            return result;
        }

        foreach (BattleMapTile tile in GetRouteFromPathMap(displayStartToGoal))
        {
            previousBattleMapTile.TryGetValue(tile, out BattleMapTile previousTile);
            costSoFar.TryGetValue(tile, out int cost);
            result += string.Format("<b>{0}</b>/{1}, ", tile.ToString(), cost);
        }
        return result;
    }
    #endregion

    #region Get Methods

    /// <summary>
    /// Get the final movement cost to reach the destination tile.
    /// If we CAN'T reach the end tile, returns int.MaxValue.
    /// </summary>
    /// <returns></returns>
    public int GetTotalCostToDestination()
    {
        int totalCost = int.MaxValue;
        costSoFar.TryGetValue(goalTile, out totalCost);
        return totalCost;
    }
    /// <summary>
    /// Returns the exact route between the Start and Goal tiles in the PathMap.
    /// If this returns NULL, then there is no route!
    /// </summary>
    /// <param name="displayStartToGoal">Determines whether the result is Start-to-Goal or Goal-to-Start.</param>
    /// <returns></returns>
    public List<BattleMapTile> GetRouteFromPathMap(bool displayStartToGoal)
    {
        List<BattleMapTile> result = new List<BattleMapTile>();

        BattleMapTile currentTile = goalTile;
        previousBattleMapTile.TryGetValue(currentTile, out BattleMapTile previousTile);
        costSoFar.TryGetValue(currentTile, out int currentCost);

        if (startTile == goalTile)
        {
            //oh hey we are already at the goal
            result.Add(goalTile);
            return result;
        }
        else if (previousTile == null)
        {
            Debug.Log("GetRouteFromPathMap: Couldn't find a route?");
            return null;
        }
        else if (currentTile == null)
        {
            Debug.Log("GetRouteFromPathMap: Couldn't find a route due to goal tile being null");
            return null;
        }

        result.Add(currentTile);

        while (previousTile != null)
        {
            currentTile = previousTile;
            result.Add(currentTile);
            previousBattleMapTile.TryGetValue(currentTile, out previousTile);
        }

        if (displayStartToGoal)
            result.Reverse();
        return result;
    }

    /// <summary>
    /// Grab every tile that's in the PathMap.
    /// </summary>
    /// <returns></returns>
    public List<BattleMapTile> GetTilesFromPathMap()
    {
        return previousBattleMapTile.Keys.ToList();
    }
    #endregion
}