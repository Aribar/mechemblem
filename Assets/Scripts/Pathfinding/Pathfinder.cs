﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

public class Pathfinder
{

    /// <summary>
    /// Removes pathfindingDistance from each tile in the map.
    /// </summary>
    /// <param name="map">The map we're clearing.</param>
    static void ClearSearch(BattleMap map)
    {
        foreach (BattleMapTile tile in map.tileData)
        {
            tile.pfDistance = int.MaxValue;
        }
    }

    /// <summary>
    /// A* heuristic for "manhattan" style distances.
    /// </summary>
    /// <param name="node">The starting location.</param>
    /// <param name="goal">The goal location.</param>
    /// <param name="cost">Nullable cost for the heuristic. Defaults to 1.</param>
    /// <returns></returns>
    static int HeuristicManhattan(Vector2Int node, Vector2Int goal, int? cost)
    {
        int dx = Mathf.Abs(node.x - goal.x);
        int dy = Mathf.Abs(node.y - goal.y);
        return (dx + dy) * (cost ?? 1);
    }

    #region Search Methods
    /// <summary>
    /// Performs an A* search to return the optimal route from a start tile to a goal tile, assuming one exists.
    /// </summary>
    /// <param name="map">The map where the search is taking place.</param>
    /// <param name="actor">The actor performing this search. Needed to consider what tiles cost.</param>
    /// <param name="start">The tile where we start the search.</param>
    /// <param name="goal">The tile we're tryingto reach.</param>
    /// <param name="getTileCost">The function to determine what new tiles will cost.</param>
    /// <param name="addTile">The function to determine if we can even walk on a tile.</param>
    /// <returns></returns>
    public static PathMap SearchAStar(BattleMap map, Actor actor, Vector2Int start, Vector2Int goal, Func<BattleMap, BattleMapTile, Actor, int> getTileCost, Func<BattleMapTile, int, bool> addTile)
    {
        ClearSearch(map);
        PriorityQueue<BattleMapTile> frontier = new PriorityQueue<BattleMapTile>();
        frontier.Enqueue(map.GetTile(start));

        PathMap path = new PathMap(map.GetTile(start), map.GetTile(goal));

        while (frontier.Count() > 0)
        {
            BattleMapTile currentTile = frontier.Dequeue();
            path.costSoFar.TryGetValue(currentTile, out int currentTileCost);

            if (currentTile.position == goal)
                break;
            foreach (BattleMapTile nextTile in map.GetTileNeighbors(currentTile.position))
            {
                int cost = getTileCost(map, nextTile, actor);
                if (cost < 0)
                    continue;

                int nextTileExistingCost = int.MaxValue;
                path.costSoFar.TryGetValue(nextTile, out nextTileExistingCost);
                int nextTileNewCost = currentTileCost + cost;

                if (!path.costSoFar.ContainsKey(nextTile) || nextTileNewCost < nextTileExistingCost)
                {
                    if(addTile(currentTile, nextTileNewCost))
                    {
                        path.costSoFar[nextTile] = nextTileNewCost;
                        nextTile.pfDistance = nextTileNewCost + HeuristicManhattan(nextTile.position, goal, 1);
                        frontier.Enqueue(nextTile);
                        path.previousBattleMapTile[nextTile] = currentTile;
                    }
                    
                }
            }
        }
        return path;
    }

    /// <summary>
    /// Return a PathMap of everything within a range given a method to calculate tile costs and determine if a tile should be added.
    /// </summary>
    /// <param name="map">The map containing the area we're searching.</param>
    /// <param name="actor">The actor performing this search. Needed to consider what tiles cost.</param>
    /// <param name="start">The initial tile from which we're searching.</param>
    /// <param name="getTileCost">How do we determine the cost of new tiles?</param>
    /// <param name="addTile">How do we determine if the unit can reach this square?</param>
    /// <returns></returns>
    public static PathMap SearchArea(BattleMap map, Actor actor, Vector2Int start, Func<BattleMap, BattleMapTile, Actor, int> getTileCost, Func<BattleMapTile, int, bool> addTile)
    {
        //Clear any previous search, prep the result, make queues for what we'll check now and next, and enqueue the starting tile.
        ClearSearch(map);
        PathMap result = new PathMap(map.GetTile(start), null); 
        Queue<BattleMapTile> checkNext = new Queue<BattleMapTile>();
        Queue<BattleMapTile> checkNow = new Queue<BattleMapTile>();
        checkNow.Enqueue(map.GetTile(start));

        //While there's still tiles in checkNow, we're going to check them to see if they should ultimately be added to the result!
        while (checkNow.Count > 0)
        {
            BattleMapTile currentTile = checkNow.Dequeue();

            //Check the neighbors of our current tile to see if they should be added.
            foreach (BattleMapTile nextTile in map.GetTileNeighbors(currentTile.position))
            {
                //Invalid tiles will have a negative cost. Otherwise, we know the tile's move cost.
                int nextCost = getTileCost(map, nextTile, actor);
                if(nextCost < 0)
                    continue;

                //If our result already has this tile, let's get its current cost! Also getting our current tile's cost.
                int nextTileExistingTotalCost = int.MaxValue;
                result.costSoFar.TryGetValue(nextTile, out nextTileExistingTotalCost);
                result.costSoFar.TryGetValue(currentTile, out int currentTileTotalCost);

                //What is our neighbor tile's new current cost if we came from this direction?
                int nextTileNewTotalCost = currentTileTotalCost + nextCost;

                //If this is brand new to the result OR it's cheaper, add it and we'll check its neighbors later!
                //... part of me wonders if that second part will be weird. Maybe just inefficient?
                if (!result.costSoFar.ContainsKey(nextTile) || nextTileNewTotalCost < nextTileExistingTotalCost)
                {
                    if (addTile(nextTile, nextTileNewTotalCost))
                    {
                        result.costSoFar[nextTile] = nextTileNewTotalCost;
                        result.previousBattleMapTile[nextTile] = currentTile;
                        checkNext.Enqueue(nextTile);
                    }
                }
            }

            //Okay, we're gonna swap and check neighboring tiles that we added.
            if (checkNow.Count == 0)
                SwapReference(ref checkNow, ref checkNext);
        }

        return result;
    }
    #endregion

    #region GetTileCost() methods
    /// <summary>
    /// Get the tile's movementCost assuming it is valid through these rules:
    ///     1. The tile exists.
    ///     2. The tile is in-bounds.
    ///     3. The tile is walkable.
    /// </summary>
    /// <param name="map">The map containing this tile.</param>
    /// <param name="tile">The tile in question.</param>
    /// <returns></returns>
    public static int GetTileCostBase(BattleMap map, BattleMapTile tile, Actor actor)
    {
        if (tile == null || !map.IsTileInBounds(tile.position) || !tile.IsWalkable(actor))
            return -1;
        //return tile.movementCost;
        return tile.terrain.movementCost;
    }

    /// <summary>
    /// Get a set movementCost of 1 for the tile assuming it is valid through these rules:
    ///     1. The tile exists.
    ///     2. The tile is in-bounds.
    /// </summary>
    /// <param name="map">The map containing this tile.</param>
    /// <param name="tile">The tile in question.</param>
    /// <returns></returns>
    public static int GetTileCostAlways1(BattleMap map, BattleMapTile tile, Actor actor)
    {
        if (tile == null || !map.IsTileInBounds(tile.position))
            return -1;
        return 1;
    }
    #endregion

    /// <summary>
    /// Used to sweap checkNow and checkNext.
    /// </summary>
    /// <param name="a">We're swapping this...</param>
    /// <param name="b">... with this!</param>
    static void SwapReference(ref Queue<BattleMapTile> a, ref Queue<BattleMapTile> b)
    {
        Queue<BattleMapTile> temp = a;
        a = b;
        b = temp;
    }

    #region Get Methods
    /// <summary>
    /// Get the tile distance between two points on the map.
    /// Note this assumes 1 tile = 1 movement cost!
    /// </summary>
    /// <param name="source">One of the points we're comparing.</param>
    /// <param name="target">The other point we're comparing.</param>
    /// <returns></returns>
    public static int GetDistanceBetweenTwoPoints(Vector2Int source, Vector2Int target)
    {
        return Mathf.Abs(source.x - target.x) + Mathf.Abs(source.y - target.y);
    }

    /// <summary>
    /// Figure out all the tiles an actor can target with weapons.
    /// </summary>
    /// <param name="map">The map we're searching.</param>
    /// <param name="destinations">The destinations where an actor could end movement.</param>
    /// <param name="actor">The actor we're investigating.</param>
    /// <returns></returns>
    static List<BattleMapTile> GetAttackableTiles(BattleMap map, List<BattleMapTile> destinations, Actor actor)
    {
        HashSet<BattleMapTile> attackableTiles = new HashSet<BattleMapTile>();

        //We're only interested in tiles attackable from just our current location.
        if(destinations.Count() == 0)
        {
            return actor.FilterTilesWithinWeaponRange(
                SearchArea(map, actor, actor.position, GetTileCostAlways1, actor.AddTileWithinMaxWeaponRange));
        }

        //We're interested in tiles attackable from our entire movement range.
        foreach (BattleMapTile destination in destinations)
        {
            attackableTiles.UnionWith(actor.FilterTilesWithinWeaponRange(
                SearchArea(map, actor, destination.position, GetTileCostAlways1, actor.AddTileWithinMaxWeaponRange)));
        }
        return attackableTiles.ToList();
    }

    /// <summary>
    /// Figure out all the tiles an actor can target with weapons.
    /// </summary>
    /// <param name="map">The map we're searching.</param>
    /// <param name="actor">The actor we're investigating.</param>
    /// <param name="onlyCurrentLocation">TRUE means get from current location, FALSE means from EVERY destination</param>
    /// <returns></returns>
    public static List<BattleMapTile> GetAttackableTiles(BattleMap map, Actor actor, bool onlyCurrentLocation)
    {
        List<BattleMapTile> destinations = new List<BattleMapTile>();
        if (!onlyCurrentLocation)
        {
            destinations = GetDestinationTiles(SearchArea(map, actor, actor.position, GetTileCostBase, actor.AddTileWithinMoveRangeStandard), actor);
        }
        
        return GetAttackableTiles(map, destinations, actor);
    }

    /// <summary>
    /// Get the movement cost distance between two points on the map.
    /// </summary>
    /// <param name="source">One of the points we're comparing.</param>
    /// <param name="destination">The other point we're comparing.</param>
    /// <param name="actor">The actor that'd be moving between the points</param>
    /// <returns></returns>
    public static int GetMovementCostBetweenTwoPoints(Vector2Int source, Vector2Int destination, Actor actor)
    {
        PathMap pathMap = SearchAStar(actor.map, actor, source, destination, GetTileCostBase, actor.AddTileEvenOutSideMoveRange);
        int result = pathMap.GetTotalCostToDestination();
        //Debug.LogFormat("Path {0} to {1} movement cost: {2}", source.ToString(), destination.ToString(), result.ToString());
        return result;
    }

    /// <summary>
    /// Determine what tiles a target can be attacked from.
    /// </summary>
    /// <param name="source">The actor that'd be attacking.</param>
    /// <param name="target">The actor we're attacking.</param>
    /// <returns></returns>
    public static Dictionary<BattleMapTile, int> GetTilesForAttackingTarget(Actor source, Actor target)
    {
        Dictionary<BattleMapTile, int> destinations = GetDestinationTilesWithCost(source);

        Dictionary<BattleMapTile, int> results = new Dictionary<BattleMapTile, int>();
        foreach (BattleMapTile destination in destinations.Keys)
        {
            List<BattleMapTile> attackableTiles = source.FilterTilesWithinWeaponRange(
                SearchArea(source.map, source, destination.position, GetTileCostAlways1, source.AddTileWithinMaxWeaponRange));

            foreach (BattleMapTile attackableTile in attackableTiles)
            {
                if (attackableTile.position == target.position)
                {
                    results.Add(destination, destinations[destination]);
                }
            }
        }
        return results;
    }

    /// <summary>
    /// Figure out all the tiles an actor where an actor can end movement.
    /// </summary>
    /// <param name="path">The PathMap we're filtering.</param>
    /// <param name="actor">The actor we're investigating.</param>
    /// <returns></returns>
    static List<BattleMapTile> GetDestinationTiles(PathMap path, Actor actor)
    {
        return actor.FilterDestinationTilesOnly(path);
    }

    /// <summary>
    /// Figure out all the tiles (+movement costs!) an actor where an actor can end movement.
    /// </summary>
    /// <param name="path">The PathMap we're filtering.</param>
    /// <param name="actor">The actor we're investigating.</param>
    /// <returns></returns>
    public static Dictionary<BattleMapTile, int> GetDestinationTilesWithCost(Actor actor)
    {
        PathMap path = SearchArea(actor.map, actor, actor.position, GetTileCostBase, actor.AddTileWithinMoveRangeStandard);
        List<BattleMapTile> tiles = GetDestinationTiles(path, actor);

        Dictionary<BattleMapTile, int> result = new Dictionary<BattleMapTile, int>();
        foreach (BattleMapTile tile in tiles)
        {
            result.Add(tile, path.costSoFar[tile]);
        }
        return result;
    }

    /// <summary>
    /// Figure out all the tiles an actor can walk through and target with weapons.
    /// </summary>
    /// <param name="map">The map we're searching.</param>
    /// <param name="actor">The actor we're investigating.</param>
    /// <param name="walkable">What tiles are walkable?</param>
    /// <param name="attackable">What tiles are attackable?</param>
    public static void GetWalkableAndAttackableTiles(BattleMap map, Actor actor, out List<BattleMapTile> walkable, out List<BattleMapTile> attackable)
    {
        PathMap path = SearchArea(map, actor, actor.position, GetTileCostBase, actor.AddTileWithinMoveRangeStandard);
        walkable = path.GetTilesFromPathMap();
        attackable = GetAttackableTiles(map, GetDestinationTiles(path, actor), actor);
    }
    #endregion
}
