﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveActorAction : IAction
{
    PathMap pathMap;

    public MoveActorAction(PathMap pathMap)
    {
        this.pathMap = pathMap;
    }

    /// <summary>
    /// Moves an actor to the goal tile as long as the following things are true:
    ///     1. Our pathmap contains a goal OR our starting position is the goal
    ///     2. There's a valid route there
    /// </summary>
    /// <param name="gm">The Game Manager.</param>
    /// <param name="entity">The entity performing the action.</param>
    public void Perform(GameManager gm, Actor actor)
    {
        if((gm.map.GetActorOnTile(pathMap.goalTile.position) == null || pathMap.startTile.position == pathMap.goalTile.position) && pathMap.HasValidRoute())
        {
            gm.selectedActor.pathMap = pathMap;
            gm.battlesm.ChangeState<PlayerMovementSequenceState>();
        }
            
    }
}
