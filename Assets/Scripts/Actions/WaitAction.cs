﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitAction : IAction
{

    public WaitAction()
    {
    }

    /// <summary>
    /// Set it so the unit can no longer act then change state.
    /// </summary>
    /// <param name="gm">The Game Manager.</param>
    /// <param name="entity">The entity performing the action.</param>
    public void Perform(GameManager gm, Actor actor)
    {
        gm.selectedActor.CanAct = false;
        gm.battlesm.ChangeState<PlayerUnitEndTurnState>();
    }
}
