using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCursorAction : IAction
{
    Vector2Int positionDelta;

    public MoveCursorAction(Vector2Int positionDelta)
    {
        this.positionDelta = positionDelta;
    }


    public void Perform(GameManager gm, Actor actor)
    {
        Vector2Int positionDestination = gm.mc.cursor.position + positionDelta;
        if(gm.map.IsTileInBounds(positionDestination))
        {
            //Move the cursor
            gm.mc.cursor.PositionMove(positionDelta);

            //If there's an actor, show the Mini Unit Panel. Otherwise, hide it.
            Actor actorUnderCursor = gm.map.GetActorOnTile(positionDestination);
            if (actorUnderCursor != null)
            {
                gm.mc.miniUnitPanel.ShowMenu(actorUnderCursor);
            }
            else
            {
                gm.mc.miniUnitPanel.HideMenu();
            }
        }
    }
}
