﻿public interface IAction
{
    void Perform(GameManager gm, Actor actor);
}