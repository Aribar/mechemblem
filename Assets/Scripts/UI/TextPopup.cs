﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextPopup : MonoBehaviour
{
    public GUIController mc;

    //References to every field in TextPopup. Set in the editor.
    [SerializeField]
    private TextMeshProUGUI text;

    public void Show(string initialText, Color color, Vector2 position)
    {
        mc.canvas.SetActive(true);

        gameObject.transform.position = position;
        text.text = initialText;
        text.color = color;
        gameObject.SetActive(true);
    }

    public void HideMenu()
    {
        gameObject.SetActive(false);
    }

}
