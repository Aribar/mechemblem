﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RoundDisplayPanel : MonoBehaviour
{
    public GUIController mc;

    //References to every field in RoundDisplayPanel. These are all set in the editor.
    [SerializeField]
    private TextMeshProUGUI roundText;
    [SerializeField]
    private Image factionImage;

    private readonly float panelAnimDuration = 2f;
    private Vector3 startPosition;
    private Vector3 endPosition;

    //hack..
    private bool firstShow = true;

    /// <summary>
    /// Show the Round Display Panel.
    /// </summary>
    /// <param name="round">What round are we on?</param>
    /// <param name="faction">What faction's phase are we in?</param>
    /// <param name="sprite">What faction's sprite are we using?</param>
    public void ShowMenu(int round, string faction, Sprite sprite)
    {
        startPosition = new Vector2(-mc.canvas.GetComponent<RectTransform>().rect.width / 2, 100);
        endPosition = new Vector2(0, 100);
        gameObject.GetComponent<RectTransform>().anchoredPosition = startPosition;

        mc.canvas.SetActive(true);
        factionImage.sprite = sprite;
        roundText.text = string.Format("Round {0}: {1} Phase", round, faction);
        gameObject.SetActive(true);

        CoroutineController.StartWatchedCoroutine(Animation(), true);
    }

    /// <summary>
    /// Animate our Round Display panel moving halfway across the screen.
    /// </summary>
    /// <returns></returns>
    private IEnumerator Animation()
    {
        //We need this because for some silly reason the very first frame has a different screen size. CANVAS SCALER!!!
        if (firstShow)
        {
            startPosition = new Vector2(-mc.canvas.GetComponent<RectTransform>().rect.width / 2, 100);
            gameObject.GetComponent<RectTransform>().anchoredPosition = startPosition;
            firstShow = false;
        }
        startPosition = new Vector2(-mc.canvas.GetComponent<RectTransform>().rect.width / 2, 100);
        yield return StartCoroutine(LerpUtility.MoveUI(gameObject, endPosition, panelAnimDuration, LerpUtility.EaseOutQuadratic));
        HideMenu();
    }

    /// <summary>
    /// Hide the Round Display Panel.
    /// </summary>
    public void HideMenu()
    {
        gameObject.SetActive(false);
    }
}
