using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCategoryPanel : MonoBehaviour
{
    public GUIController mc;
    List<MenuEntry> entries = new List<MenuEntry>();
    public int Selection { get; private set; }


    #region Menu Show/Hide/Clear
    /// <summary>
    /// Show the Action Category Panel with a list of buttons.
    /// </summary>
    /// <param name="options">Labels for the buttons.</param>
    public void ShowMenu(List<string> options)
    {
        mc.canvas.SetActive(true);
        Clear();

        foreach(string option in options)
        {
            MenuEntry entry = Dequeue();
            entry.Label = option;
            entries.Add(entry);
        }
        gameObject.SetActive(true);
        SetSelection(0);
    }

    /// <summary>
    /// Clear everything from the Action Category Panel and hide it.
    /// </summary>
    public void HideMenu()
    {
        Clear();
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Add everything in entries back to the Pool and clear it.
    /// </summary>
    void Clear()
    {
        foreach(MenuEntry entry in entries)
        {
            Enqueue(entry);
        }
        entries.Clear();
    }
    #endregion

    #region Selection-related
    /// <summary>
    /// Return the label of button we're selecting.
    /// </summary>
    /// <returns></returns>
    public string GetSelection()
    {
        return entries[Selection].Label.ToString();
    }

    /// <summary>
    /// Move to an adjacent menu entry.
    /// </summary>
    /// <param name="next">If true, go "down". If false, go "up".</param>
    public void IncrementSelection(bool next)
    {
        int index;
        if (next)
            index = (Selection + 1) % entries.Count;
        else
            index = (Selection - 1 + entries.Count) % entries.Count;
        SetSelection(index);
    }
    /// <summary>
    /// Deselect previous entry and select the new entry.
    /// </summary>
    /// <param name="value">Entry to be selected.</param>
    void SetSelection(int value)
    {
        if (Selection >= 0 && Selection < entries.Count)
            entries[Selection].IsSelected = false;
        Selection = value;
        if (Selection >= 0 & Selection < entries.Count)
            entries[Selection].IsSelected = true;
    }
    #endregion

    #region Pool-related
    /// <summary>
    /// Grab a MenuEntry from the pool, make this its parent, and make it properly positioned.
    /// </summary>
    /// <returns></returns>
    public MenuEntry Dequeue()
    {
        GameObject go = PoolController.Dequeue("ActionCategoryEntry").gameObject;
        MenuEntry entry = go.GetComponent<MenuEntry>();
        go.transform.SetParent(gameObject.transform, false);
        //Do I need the local scale thing?
        go.SetActive(true);

        entry.Reset();
        return entry;
    }

    /// <summary>
    /// Add a MenuEntry back to the Pool.
    /// </summary>
    /// <param name="entry">The MenuEntry we're adding to the Pool.</param>
    void Enqueue(MenuEntry entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        PoolController.Enqueue(p);
    }
    #endregion
}
