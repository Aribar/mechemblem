using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIController : MonoBehaviour
{
    public GameObject canvas; //Set in the editor.
    //Prefabs
    public GameObject actionCategoryPanelPrefab; //Set in the editor.
    public ActionCategoryPanel actionCategoryPanel;
    public GameObject actionCategoryButtonPrefab; //Set in the editor.

    public GameObject combatInfoPanelPrefab; //Set in the editor.
    public CombatInfoPanel combatInfoPanel;

    public GameObject roundDisplayPanelPrefab; //Set in the editor.
    public RoundDisplayPanel roundDisplayPanel;

    public GameObject miniUnitPanelPrefab; //Set in the editor.
    public MiniUnitPanel miniUnitPanel;

    public GameObject healthPanelContainerPrefab; //Set in the editor.
    public HealthPanelContainer healthPanelContainer;
    public GameObject healthPanelPrefab; //Set in the editor.

    public GameObject textPopupPrefab; //Set in the editor.
    public TextPopup textPopup;


    //Cursor
    public Entity cursor;

    private void Awake()
    {
        Canvas.ForceUpdateCanvases();
        canvas.SetActive(false);
        //Set up Action Category Panel and its buttons
        actionCategoryPanel = Instantiate(actionCategoryPanelPrefab, canvas.transform).GetComponent<ActionCategoryPanel>();
        actionCategoryPanel.mc = this;
        PoolController.AddEntry("ActionCategoryEntry", actionCategoryButtonPrefab, 1, int.MaxValue);
        actionCategoryPanel.gameObject.SetActive(false);

        //Set up Combat Info Panel
        combatInfoPanel = Instantiate(combatInfoPanelPrefab, canvas.transform).GetComponent<CombatInfoPanel>();
        combatInfoPanel.mc = this;
        combatInfoPanel.gameObject.SetActive(false);

        //Set up the Round Info Panel
        roundDisplayPanel = Instantiate(roundDisplayPanelPrefab, canvas.transform).GetComponent<RoundDisplayPanel>();
        roundDisplayPanel.mc = this;
        roundDisplayPanel.gameObject.SetActive(false);

        //Set up the Mini Unit Panel
        miniUnitPanel = Instantiate(miniUnitPanelPrefab, canvas.transform).GetComponent<MiniUnitPanel>();
        miniUnitPanel.mc = this;
        miniUnitPanel.gameObject.SetActive(false);

        //Set up the Health Panel Container and its panels
        healthPanelContainer = Instantiate(healthPanelContainerPrefab, canvas.transform).GetComponent<HealthPanelContainer>();
        healthPanelContainer.mc = this;
        healthPanelContainer.gameObject.SetActive(false);
        PoolController.AddEntry("HealthPanelEntry", healthPanelPrefab, 1, int.MaxValue);

        //Set up Text Popups.
        textPopup = Instantiate(textPopupPrefab, canvas.transform).GetComponent<TextPopup>();
        textPopup.mc = this;
        textPopup.gameObject.SetActive(false);
        //PoolController.AddEntry("TextPopup", textPopupPrefab, 1, int.MaxValue);
    }
}
