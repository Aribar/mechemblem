﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthPanelContainer : MonoBehaviour
{
    public GUIController mc;
    List<HealthPanel> panels = new List<HealthPanel>();

    public void ShowMenu(List<Actor> actors)
    {
        mc.canvas.SetActive(true);
        Clear();

        foreach(Actor actor in actors)
        {
            AddPanel(actor);
        }
        gameObject.SetActive(true);
    }

    public void ShowMenuEmpty()
    {
        mc.canvas.SetActive(true);
        Clear();
        gameObject.SetActive(true);
    }

    public void HideMenu()
    {
        Clear();
        gameObject.SetActive(false);
    }

    void Clear()
    {
        foreach(HealthPanel panel in panels)
        {
            Enqueue(panel);
        }
        panels.Clear();
    }

    public void AddPanel(Actor actor)
    {
        HealthPanel panel = Dequeue();
        panel.ShowMenu(actor);
        panels.Add(panel);
    }

    public void UpdatePanelCurrentHealth(Actor actor, int newHealth)
    {
        panels.Find(x => x.actor == actor).UpdateHealth(newHealth);
    }


    public HealthPanel Dequeue()
    {
        GameObject go = PoolController.Dequeue("HealthPanelEntry").gameObject;
        HealthPanel panel = go.GetComponent<HealthPanel>();
        go.transform.SetParent(gameObject.transform, false);
        go.SetActive(true);

        return panel;
    }

    void Enqueue(HealthPanel panel)
    {
        Poolable p = panel.GetComponent<Poolable>();
        PoolController.Enqueue(p);
    }
}
