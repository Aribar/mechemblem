﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthPanel : MonoBehaviour
{
    //public GUIController mc;

    //References to every field in MiniUnitPanel. These are all set in the editor.
    [SerializeField]
    private TextMeshProUGUI unitNameText;
    [SerializeField]
    private TextMeshProUGUI healthText;

    public Actor actor;

    Color panelBackgroundDefault = new Color(1, 1, 1, 1);
    Color panelBackgroundEnemy = new Color(1f, .2f, .2f, 1);

    /// <summary>
    /// Show the Round Display Panel.
    /// </summary>
    /// <param name="round">What round are we on?</param>
    /// <param name="faction">What faction's phase are we in?</param>
    /// <param name="sprite">What faction's sprite are we using?</param>
    public void ShowMenu(Actor actor)
    {
        this.actor = actor;
        if (!actor.faction.playerControlled)
        {
            GetComponent<Image>().color = panelBackgroundEnemy;
        }
        else
        {
            GetComponent<Image>().color = panelBackgroundDefault;
        }

        //mc.canvas.SetActive(true);
        unitNameText.text = actor.name;
        Weapon weapon = actor.GetCurrentWeapon();
        healthText.text = string.Format("HP: {0} / {1}", actor.HpCurrent, actor.hpMax);
        gameObject.SetActive(true);
    }

    public void UpdateHealth(int newHealth)
    {
        healthText.text = string.Format("HP: {0} / {1}", newHealth, actor.hpMax);
    }

    /// <summary>
    /// Hide the Round Display Panel.
    /// </summary>
    public void HideMenu()
    {
        gameObject.SetActive(false);
    }
}
