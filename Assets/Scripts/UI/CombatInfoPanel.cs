﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CombatInfoPanel : MonoBehaviour
{
    public GUIController mc;
    Actor attacker;

    //References to every field in the CombatInfoPanel. These are all set in the editor.
    [SerializeField]
    private TextMeshProUGUI nameAttacker;
    [SerializeField]
    private TextMeshProUGUI weaponAttacker;
    [SerializeField]
    private TextMeshProUGUI hpAttacker;
    [SerializeField]
    private TextMeshProUGUI damageAttacker;
    [SerializeField]
    private TextMeshProUGUI hitAttacker;
    [SerializeField]
    private TextMeshProUGUI nameDefender;
    [SerializeField]
    private TextMeshProUGUI weaponDefender;
    [SerializeField]
    private TextMeshProUGUI hpDefender;
    [SerializeField]
    private TextMeshProUGUI damageDefender;
    [SerializeField]
    private TextMeshProUGUI hitDefender;

    //List of targeted actors and the currently selected target.
    public List<Actor> targetedActors = new List<Actor>();
    public int targetedActorsSelection = 0;

    #region TextMeshProUGUI Getters/Setters
    public string NameAttacker
    {
        get { return nameAttacker.text; }
        set { nameAttacker.text = value; }
    }

    public string WeaponAttacker
    {
        get { return weaponAttacker.text; }
        set { weaponAttacker.text = value; }
    }

    public string HPAttacker
    {
        get { return hpAttacker.text; }
        set { hpAttacker.text = value; }
    }

    public string DamageAttacker
    {
        get { return damageAttacker.text; }
        set { damageAttacker.text = value; }
    }

    public string HitAttacker
    {
        get { return hitAttacker.text; }
        set { hitAttacker.text = value; }
    }

    public string NameDefender
    {
        get { return nameDefender.text; }
        set { nameDefender.text = value; }
    }

    public string WeaponDefender
    {
        get { return weaponDefender.text; }
        set { weaponDefender.text = value; }
    }

    public string HPDefender
    {
        get { return hpDefender.text; }
        set { hpDefender.text = value; }
    }

    public string DamageDefender
    {
        get { return damageDefender.text; }
        set { damageDefender.text = value; }
    }

    public string HitDefender
    {
        get { return hitDefender.text; }
        set { hitDefender.text = value; }
    }
    #endregion

    #region Show/Update/Hide Menu
    /// <summary>
    /// Show the Combat Info Panel.
    /// </summary>
    /// <param name="attacker">Actor initiating the attack.</param>
    /// <param name="defender">Actor receiving the attack.</param>
    public void ShowMenu(Actor attacker)
    {
        this.attacker = attacker;
        //Gonna sort all the targets
        SortTargetedActorsByYThenX();

        mc.canvas.SetActive(true);

        UpdateMenuInfo(attacker, GetCurrentTargetedActor());

        gameObject.SetActive(true);

        mc.cursor.PositionSet(GetCurrentTargetedActor().position, true);
    }

    /// <summary>
    /// Update the Combat Info Panel
    /// </summary>
    /// <param name="attacker">Actor initiating the attack.</param>
    /// <param name="defender">Actor receiving the attack.</param>
    void UpdateMenuInfo(Actor attacker, Actor defender)
    {
        NameAttacker = attacker.name;
        WeaponAttacker = attacker.weapons[attacker.selectedWeapon].name;
        HPAttacker = string.Format("{0}/{1}", attacker.HpCurrent, attacker.hpMax);
        DamageAttacker = attacker.weapons[attacker.selectedWeapon].damage.ToString();
        HitAttacker = attacker.weapons[attacker.selectedWeapon].hit.ToString();

        NameDefender = defender.name;
        WeaponDefender = defender.weapons[defender.selectedWeapon].name;
        HPDefender = string.Format("{0}/{1}", defender.HpCurrent, defender.hpMax);

        //Only show counterattack info if the defender can counter!
        if (defender.weapons[defender.selectedWeapon].CanTargetTile(defender.position, attacker.position))
        {
            DamageDefender = defender.weapons[defender.selectedWeapon].damage.ToString();
            HitDefender = defender.weapons[defender.selectedWeapon].hit.ToString();
        }
        else
        {
            DamageDefender = "--";
            HitDefender = "--";
        }
    }

    /// <summary>
    /// Hide the Combat Info Panel.
    /// </summary>
    public void HideMenu()
    {
        gameObject.SetActive(false);
        targetedActorsSelection = 0;
    }
    #endregion

    /// <summary>
    /// Return the currently targeted actor.
    /// </summary>
    /// <returns></returns>
    public Actor GetCurrentTargetedActor()
    {
        return targetedActors[targetedActorsSelection];
    }

    /// <summary>
    /// Select the next enemy in the list, move the cursor there, update panel.
    /// </summary>
    /// <param name="next">If true, go "down". If false, go "up".</param>
    public void IncrementSelection(bool next)
    {
        int index;
        if (next)
            index = (targetedActorsSelection + 1) % targetedActors.Count;
        else
            index = (targetedActorsSelection - 1 + targetedActors.Count) % targetedActors.Count;
        targetedActorsSelection = index;
        mc.cursor.PositionSet(targetedActors[targetedActorsSelection].position, true);
        UpdateMenuInfo(attacker, targetedActors[targetedActorsSelection]);
    }

    /// <summary>
    /// Sort TargetedActors first by y (highest first) then x (lowest first).
    /// </summary>
    public void SortTargetedActorsByYThenX()
    {
        targetedActors.Sort(delegate (Actor a, Actor b)
            {
                if(a.position.y > b.position.y)
                {
                    return -1;
                }
                else if (a.position.y == b.position.y)
                {
                    if (a.position.x < b.position.x)
                    {
                        return -1;
                    }
                }
                return 1;
            });
    }
}