using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//The joy of Bitwise stuff: https://www.techotopia.com/index.php/Objective-C_Operators_and_Expressions
public class MenuEntry : MonoBehaviour
{
    [System.Flags]
    public enum EntryState
    {
        Unselected = 0,
        Selected = 1 << 0 //Bitwise operator
    }

    [SerializeField]
    private TextMeshProUGUI label;
    private EntryState state;
    
    public string Label
    {
        get { return label.text; }
        set { label.text = value; }
    }

    public EntryState State
    {
        get { return state; }
        set
        {
            if (state == value)
                return;
            state = value;

            Image img = gameObject.GetComponent<Image>();
            if (IsSelected)
            {
                img.color = new Color32(255, 103, 7, 255);
                label.color = new Color32(255, 239, 173, 255);
            }
            else
            {
                img.color = new Color32(255, 239, 173, 255);
                label.color = Color.gray;
            }
        }
    }

    /// <summary>
    /// Controls whether the MenuEntry is currently selected or not.
    /// </summary>
    public bool IsSelected
    {
        get { return (State & EntryState.Selected) != EntryState.Unselected; }
        set
        {
            if (value)
                State |= EntryState.Selected; //Assign to x the result of logical OR operation on x and y
            else
                State &= ~EntryState.Selected; //Assign to x the result of logical AND operation on x and y, ~ reverses bits.
        }
    }

    /// <summary>
    /// Forces MenuEntry to be unselected.
    /// </summary>
    public void Reset()
    {
        State = EntryState.Unselected;
    }

}
