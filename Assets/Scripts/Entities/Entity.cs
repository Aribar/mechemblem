using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    //public string name;
    public Vector2Int position { get; private set; }
    public BattleMap map;

    #region Entity Creation Stuff
    /// <summary>
    /// Add and configure necessary parts of an Entity.
    /// </summary>
    /// <param name="position">The starting position of the Entity.</param>
    /// <param name="sprite">The sprite the Entity will use.</param>
    protected void InitializeEntity(BattleMap map, Vector2Int position, Sprite sprite)
    {
        this.map = map;
        PositionSet(position, true);

        gameObject.AddComponent<SpriteRenderer>();
        gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "Entity";
    }

    /// <summary>
    /// Create a GameObject with the Entity component and then call InitializeEntity().
    /// </summary>
    /// <param name="name">The name of the created GameObject.</param>
    /// <param name="position">The starting position of the created GameObject.</param>
    /// <param name="sprite">The sprite the Entity will use.</param>
    /// <returns></returns>
    public static Entity CreateEntity(string name, BattleMap map, Vector2Int position, Sprite sprite)
    {
        Entity entity = new GameObject(name).AddComponent<Entity>();
        entity.InitializeEntity(map, position, sprite);
        return entity;
    }
    #endregion

    /// <summary>
    /// Move the entity's current position by the amount in newPosition.
    /// </summary>
    /// <param name="newPosition">How far the entity will move.</param>
    public void PositionMove(Vector2Int newPosition)
    {
        position += newPosition;
        gameObject.transform.position = new Vector3(position.x, position.y, 0);
    }

    /// <summary>
    /// Set the entity's current position to newPosition.
    /// </summary>
    /// <param name="newPosition">The entity's new location.</param>
    /// <param name="updateTransform">Should the transform of the entity's GameObject update?</param>
    public void PositionSet(Vector2Int newPosition, bool updateTransform)
    {
        position = newPosition;
        if (updateTransform)
        {
            gameObject.transform.position = new Vector3(position.x, position.y, 0);
        }
    }
}
