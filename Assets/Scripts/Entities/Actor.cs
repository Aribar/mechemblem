﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Actor : Entity
{
    //Faction, Acting, and AI
    public BaseAI ai;
    public Faction faction;

    private bool canAct = true;
    public bool isAlive = true;

    //Movement-related
    public int moveRange;
    public MoveType moveType;
    public PathMap pathMap;


    //Movement animation stuff?
    private readonly float moveAnimDuration = .2f;
    private readonly float attackAnimDuration = .0625f;
    public bool moveQuick = false;

    //Stats
    public int hpMax = 20;
    private int hpCurrent = 20;
    public int HpCurrent
    {
        get { return hpCurrent; }
        set
        {
            hpCurrent = value;
            if (hpCurrent <= 0)
            {
                hpCurrent = 0;
                isAlive = false;
            }
        }
    }

    //Equipment
    public List<Weapon> weapons = new List<Weapon>();
    public int selectedWeapon = 0;

    //Determines if a unit can act during its turn.
    public bool CanAct
    {
        get { return canAct; }
        set
        {
            if (canAct == value)
                return;
            canAct = value;

            SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
            if (canAct)
            {
                renderer.color = new Color(1f, 1f, 1f);
            }
            else
            {
                renderer.color = new Color(.25f, .25f, .25f);
            }
        }
    }

    #region Actor Creation Stuff
    /// <summary>
    /// Add and configure necessary parts of an Actor, including Entity stuff.
    /// </summary>
    /// <param name="position">The starting position of the Actor.</param>
    /// <param name="sprite">The sprite the Actor will use.</param>
    /// <param name="moveRange">The movement range of the Actor.</param>
    protected void InitializeActor(Vector2Int position, Sprite sprite, int moveRange, BattleMap map)
    {
        this.moveRange = moveRange;
        InitializeEntity(map, position, sprite);
        ai = new BaseAI(this);
    }

    /// <summary>
    /// Create a GameObject with the Actor component and then call InitializeActor().
    /// </summary>
    /// <param name="name">The name of the created GameObject.</param>
    /// <param name="position">The starting position of the created GameObject.</param>
    /// <param name="sprite">The sprite the Actor will use.</param>
    /// <param name="moveRange">The movement range of the Actor.</param>
    /// <returns></returns>
    public static Actor CreateActor(string name, Vector2Int position, Sprite sprite, int moveRange, BattleMap map)
    {
        Actor actor = new GameObject(name).AddComponent<Actor>();
        actor.InitializeActor(position, sprite, moveRange, map);

        return actor;
    }
    #endregion

    /// <summary>
    /// Return the currently equipped weapon.
    /// </summary>
    /// <returns></returns>
    public Weapon GetCurrentWeapon()
    {
        return weapons[selectedWeapon];
    }

    #region Add Tile Methods
    /// <summary>
    /// Add a tile meeting that...
    ///     1. Is walkable by this actor
    ///     2. Is either unoccupied by an actor OR is has an actor of this actor's factor.
    ///     3. Is within this actor's MoveRange.
    /// </summary>
    /// <param name="to">The tile we're potentially adding.</param>
    /// <param name="totalCost">The sum of movement costs to move here.</param>
    /// <returns></returns>
    public bool AddTileWithinMoveRangeStandard(BattleMapTile to, int totalCost)
    {
        if (to.IsWalkable(this) && (to.GetActorOnTile() == null || (faction == to.GetActorOnTile().faction)) && totalCost <= moveRange)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Add a tile that...
    ///     1. Is walkable by this actor
    ///     2. Is either unoccupied by an actor OR is has an actor of this actor's factor.
    /// </summary>
    /// <param name="to">The tile we're potentially adding</param>
    /// <param name="totalCost">Unused, but needed to match the function needed.</param>
    /// <returns></returns>
    public bool AddTileEvenOutSideMoveRange(BattleMapTile to, int totalCost)
    {
        if (to.IsWalkable(this) && (to.GetActorOnTile() == null || (faction == to.GetActorOnTile().faction)))
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Pathfinder helper. Allows a tile to be considered as long as it is within the max range of your weapon.
    /// </summary>
    /// <param name="to">The tile we're potentially adding.</param>
    /// <param name="totalCost">The sum of movement costs to move here.</param>
    /// <returns></returns>
    public bool AddTileWithinMaxWeaponRange(BattleMapTile to, int totalCost)
    {
        if (totalCost <= weapons[selectedWeapon].rangeMax)
            return true;
        else
            return false;
    }
    #endregion

    #region Filter PathMap Methods
    /// <summary>
    /// Kinda Pathfinder helper? Will filter a PathMap to only consider tiles within your weapon's range.
    /// </summary>
    /// <param name="map">The pathmap being considered.</param>
    /// <returns></returns>
    public List<BattleMapTile> FilterTilesWithinWeaponRange(PathMap map)
    {
        List<BattleMapTile> result = new List<BattleMapTile>();

        foreach(BattleMapTile tile in map.previousBattleMapTile.Keys)
        {
            if (map.costSoFar[tile] >= weapons[selectedWeapon].rangeMin && map.costSoFar[tile] <= weapons[selectedWeapon].rangeMax)
                result.Add(tile);
        }
        return result;
    }


    /// <summary>
    /// We should only return tiles that can actually be moved to!
    /// There may be a better way to get this info...
    /// </summary>
    /// <param name="map">The pathmap being considered.</param>
    /// <returns></returns>
    public List<BattleMapTile> FilterDestinationTilesOnly(PathMap map)
    {
        List<BattleMapTile> result = new List<BattleMapTile>();

        foreach (BattleMapTile tile in map.previousBattleMapTile.Keys)
        {
            if(tile.GetActorOnTile() == null || tile.GetActorOnTile() == this)
            {
                result.Add(tile);
            }
        }

        return result;
    }
    #endregion

    #region Movement animation methods
    /// <summary>
    /// Moves the actor along its PathMap to the goal.
    /// </summary>
    /// <returns></returns>
    public IEnumerator MoveAlongRoute()
    {
        BattleMapTile currentTile = pathMap.startTile;
        foreach (BattleMapTile nextTile in pathMap.GetRouteFromPathMap(true))
        {
            if (nextTile == pathMap.startTile)
                continue;
            yield return StartCoroutine(LerpUtility.MoveActor(this, nextTile.position, moveAnimDuration, LerpUtility.Linear));
            currentTile = nextTile;
        }
        yield return null;
        PositionSet(pathMap.goalTile.position, true);
    }

    /// <summary>
    /// This is actually effectively "return to Entity.position".
    /// </summary>
    /// <returns></returns>
    public IEnumerator MoveFromAttack()
    {
        yield return LerpUtility.MoveActor(this, position, attackAnimDuration, LerpUtility.Linear);
    }

    /// <summary>
    /// Move just slightly towards our target!
    /// </summary>
    /// <param name="target">The Actor we're moving towards.</param>
    /// <returns></returns>
    public IEnumerator MoveToAttack (Actor target)
    {
        Vector2 forwardSpot = Vector3.MoveTowards(gameObject.transform.position, target.transform.position, .3f);
        yield return LerpUtility.MoveActor(this, forwardSpot, attackAnimDuration, LerpUtility.Linear);
    }
    #endregion
}