﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Weapon", order = 1)]
public class Weapon : ScriptableObject
{
    public string weaponName;
    public int damage;
    public int hit;
    public int rangeMin;
    public int rangeMax;

    //Eventually need effects.

    public Weapon(string weaponName, int damage, int hit, int rangeMin, int rangeMax)
    {
        this.weaponName = weaponName;
        this.damage = damage;
        this.hit = hit;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
    }

    /// <summary>
    /// Returns if this weapon can target the tile.
    /// </summary>
    /// <param name="source">Where the weapon is being used.</param>
    /// <param name="target">Where the weapon is targeting.</param>
    /// <returns></returns>
    public bool CanTargetTile(Vector2Int source, Vector2Int target)
    {
        int distance = Pathfinder.GetDistanceBetweenTwoPoints(source, target);
        return distance >= rangeMin && distance <= rangeMax;
    }
}