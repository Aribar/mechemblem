﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WeaponFactory
{
    public List<Weapon> weapons;

    public void PopulateFactory()
    {
        weapons = Resources.LoadAll<Weapon>("Data/Weapons").ToList();
    }
}