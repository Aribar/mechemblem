﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// This is an overall Tiled map file.
/// </summary>
[Serializable]
public class TiledFile
{
    public TiledLayer [] layers;
    public TiledTileset[] tilesets;

    public TiledLayer MapLayer { get { return layers.First(x => x.name == "MapLayer"); } }

    /// <summary>
    /// Given a tileID, find the relevant terrain's name from the TiledFile.
    /// </summary>
    /// <param name="tileID">The tileID we're finding info on.</param>
    /// <returns></returns>
    public string GetTerrainNameForTile(int tileID)
    {
        foreach(TiledTileset tileset in tilesets)
        {
            if(tileset != null && tileset.firstgid-1 <= tileID)
            {
                TiledTile tile = tileset.tiles.First(x => x.id == tileID);
                if (tile != null && tile.properties != null)
                {
                    return tile.properties.First(x => x.name == "Name").value;
                }
            }
        }
        return null;
    }
}

/// <summary>
/// This is one map layers in the map file.
/// </summary>
[Serializable]
public class TiledLayer
{
    public string [] data;
    public int height;
    public string name;
    public int width;
}

/// <summary>
/// This is one of the tilesets in the map file.
/// </summary>
[Serializable]
public class TiledTileset
{
    public int firstgid;
    public string name;
    public TiledTile[] tiles;
}

/// <summary>
/// This is a tile of a tileset.
/// </summary>
[Serializable]
public class TiledTile
{
    public int id;
    public TiledTileProperties[] properties;
    public string type;
}

/// <summary>
/// This is a property of a tile.
/// </summary>
[Serializable]
public class TiledTileProperties
{
    public string name;
    public string value;
}