using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class BattleMap
{
    public GameManager gm;
    public Tilemap tileMap;
    public BattleMapTile[,] tileData;

    public int height;
    public int width;

    public List<Entity> entities = new List<Entity>();
    public List<Actor> actors = new List<Actor>();

    //Colors!
    Color tileColorDefault = new Color(1, 1, 1, 1);
    Color tileColorMovement = new Color(0, .2f, 1f, 1);
    Color tileColorAttack = new Color(1f, .2f, .2f, 1);

    /// <summary>
    /// Convenience variable to help identify the 4 neighbors of a tile.
    /// </summary>
    readonly Vector2Int[] neighbors = new Vector2Int[4]
    {
        new Vector2Int(0, 1),
        new Vector2Int(0, -1),
        new Vector2Int(1, 0),
        new Vector2Int(-1, 0)
    };

    public BattleMap(GameManager gm, string mapName)
    {
        this.gm = gm;

        //Load the map from the given mapName.
        TextAsset fileText = Resources.Load<TextAsset>("Data/Maps/" + mapName);
        if (fileText != null)
        {
            TiledFile fileData = JsonUtility.FromJson<TiledFile>(fileText.ToString());

            //Find the size of the map prior to use initializing some tileData.
            height = fileData.MapLayer.height;
            width = fileData.MapLayer.width;
            tileData = new BattleMapTile[width, height];

            for (int y = height - 1; y >= 0; y--)
            {
                for (int x = 0; x < width; x++)
                {
                    int fileDataIndex = (height - 1 - y) * width + x;
                    int tileID = int.Parse(fileData.MapLayer.data[fileDataIndex]) - 1;
                    //Try to get the terrain data for this tile! If we can't find it, just load a blank terrain.
                    Terrain terrain = Resources.Load<Terrain>("Data/Terrain/" + fileData.GetTerrainNameForTile(tileID));
                    if (terrain == null)
                    {
                        Debug.LogErrorFormat("In Map 'Data/Maps/{0}' index {1} had tileID {2}. Could not find terrain 'Data/Terrain/{3}'!", mapName, fileDataIndex, tileID, fileData.GetTerrainNameForTile(tileID));
                        terrain = Resources.Load<Terrain>("Data/Terrain/Blank");
                    }

                    string tileBaseName = "Tiles/TestMap/MechEmblemTestTileset_" + tileID;
                    tileData[x, y] = new BattleMapTile(this, new Vector2Int(x, y), Resources.Load<TileBase>(tileBaseName), terrain);
                }
            }

            //Make game objects for rendering.
            GameObject grid = new GameObject("Grid");
            grid.AddComponent<Grid>();
            tileMap = CreateTileMap("BattleTileMap", grid.GetComponent<Grid>(), "MapFloor");
        }
        else
        {
            Debug.LogErrorFormat("Map 'Data/Maps/{0}' does not exist!", mapName);
        }
    }

    #region Initialization and Rendering Methods
    /// <summary>
    /// Create the TileMap for the map graphics.
    /// </summary>
    /// <param name="name">The name of the TileMap GameObject.</param>
    /// <param name="grid">The TileMap's parent Grid.</param>
    /// <param name="sortingLayer">The TileMap's sorting layer</param>
    /// <returns></returns>
    Tilemap CreateTileMap(string name, Grid grid, string sortingLayer)
    {
        GameObject tileMapGameObject = new GameObject(name);

        tileMapGameObject.transform.SetParent(grid.transform);
        Tilemap returnedTileMap = tileMapGameObject.AddComponent<Tilemap>();
        returnedTileMap.tileAnchor = new Vector3(.5f, .5f, 0);

        tileMapGameObject.AddComponent<TilemapRenderer>();
        tileMapGameObject.GetComponent<TilemapRenderer>().sortingLayerName = sortingLayer;
        return returnedTileMap;
    }

    /// <summary>
    /// Actually add graphics to the TileMap.
    /// </summary>
    public void RenderMap()
    {
        Vector3Int[] positions = new Vector3Int[width * height];
        TileBase[] floorTileArray = new TileBase[positions.Length];

        //Set the tileBase for each position on the map then set them to the tileMap.
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                //Rendering the "Floor" portion
                int index = x * height + y;
                positions[index] = new Vector3Int(x, y, 0);
                floorTileArray[index] = tileData[x, y].tileBase;
            }
        }
        tileMap.SetTiles(positions, floorTileArray);
        
        //Change the dumb TileFlags thing to None so we can color things.
        foreach(BattleMapTile tile in tileData)
        {
            tileMap.SetTileFlags(tile.ToVector3Int(), TileFlags.None);
        }
    }
    #endregion

    #region Get Methods
    /// <summary>
    /// Find the first living Actor on a tile or return null if there is none.
    /// </summary>
    /// <param name="position">The Position we are checking.</param>
    /// <returns></returns>
    public Actor GetActorOnTile(Vector2Int position)
    {
        return actors.Find(x => x.position == position && x.isAlive);
    }

    /// <summary>
    /// Find the first Entity on a tile or return null if there is none.
    /// </summary>
    /// <param name="position">The Position we are checking.</param>
    /// <returns></returns>
    public Entity GetEntityOnTile(Vector2Int position)
    {
        return entities.Find(x => x.position == position);
    }

    /// <summary>
    /// Retrieve the BattleMapTile for a certain position.
    /// </summary>
    /// <param name="position">The position we're examining.</param>
    /// <returns></returns>
    public BattleMapTile GetTile(Vector2Int position)
    {
        if (IsTileInBounds(position))
            return tileData[position.x, position.y];
        return null;
    }

    /// <summary>
    /// Returns a list of the neighboring tiles to the given position.
    /// </summary>
    /// <param name="position">The position we're checking.</param>
    /// <returns></returns>
    public List<BattleMapTile> GetTileNeighbors(Vector2Int position)
    {
        List<BattleMapTile> result = new List<BattleMapTile>();
        for (int i = 0; i < 4; i++)
        {
            result.Add(GetTile(position + neighbors[i]));
        }
        return result;
    }
    #endregion

    #region TilesActorsThreaten Methods
    /// <summary>
    /// Removes all actors from every tile's targetingActors list.
    /// </summary>
    public void ClearTilesActorsThreaten()
    {
        foreach(BattleMapTile tile in tileData)
        {
            tile.targetingActors.Clear();
        }
    }

    /// <summary>
    /// For every tile on the map determine what actors threaten it.
    /// </summary>
    public void DetermineTilesActorsThreaten()
    {
        foreach (Actor actor in actors)
        {
            if(actor.isAlive)
            {
                foreach (BattleMapTile tile in Pathfinder.GetAttackableTiles(this, actor, false))
                {
                    tile.targetingActors.Add(actor);
                }
            }
        }
    }
    #endregion

    /// <summary>
    /// Is the position in question on the map?
    /// </summary>
    /// <param name="position">The Position we are checking.</param>
    /// <returns></returns>
    public bool IsTileInBounds(Vector2Int position)
    {
        return (0 <= position.x && position.x < width &&
                0 <= position.y && position.y < height);
    }

    #region Tile Highlighting
    /// <summary>
    /// Change the tiles' color to selectedTileColor.
    /// </summary>
    /// <param name="tiles">The tiles to be highlighted.</param>
    /// <param name="highlight">The type of highlight.</param>
    public void AddHighlightToTiles(List<BattleMapTile> tiles, string highlight)
    {
        foreach(BattleMapTile tile in tiles)
        {
            switch(highlight)
            {
                case "Movement":
                    tileMap.SetColor(tile.ToVector3Int(), tileColorMovement);
                    break;
                case "Attack":
                    tileMap.SetColor(tile.ToVector3Int(), tileColorAttack);
                    break;
            }                
        }
    }

    /// <summary>
    /// Changes the tiles' color to defaultTilecolor.
    /// </summary>
    /// <param name="tiles">The tiles to be unhighlighted.</param>
    public void RemoveHighlightFromTiles(List<BattleMapTile> tiles)
    {
        foreach (BattleMapTile tile in tiles)
        {
            tileMap.SetColor(tile.ToVector3Int(), tileColorDefault);
        }
    }
    #endregion
}