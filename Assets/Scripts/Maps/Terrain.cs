﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Terrain", order = 2)]
public class Terrain : ScriptableObject
{
    public string terrainName;
    public int armorModifier;
    public int avoidModifier;
    public int movementCost;
    public MovementAllowed movementAllowed;

    public Terrain(string terrainName, int armorModifier, int avoidModifier, int movementCost, MovementAllowed movementAllowed)
    {
        this.terrainName = terrainName;
        this.armorModifier = armorModifier;
        this.avoidModifier = avoidModifier;
        this.movementCost = movementCost;
        this.movementAllowed = movementAllowed;
    }
}