﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TerrainFactory
{
    public List<Terrain> terrain;

    public void PopulateFactory()
    {
        terrain = Resources.LoadAll<Terrain>("Data/Terrain").ToList();
    }
}