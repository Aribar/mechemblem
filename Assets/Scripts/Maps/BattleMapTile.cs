using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BattleMapTile : IComparable<BattleMapTile>
{
    public Vector2Int position;
    public TileBase tileBase;
    public BattleMap map;
    public Terrain terrain;

    public int pfDistance = int.MaxValue;

    public List<Actor> targetingActors = new List<Actor>();

    #region Constructors
    public BattleMapTile() { }

    public BattleMapTile(BattleMap map, Vector2Int position, TileBase tileBase, Terrain terrain)
    {
        this.map = map;
        this.position = position;
        this.tileBase = tileBase;
        this.terrain = terrain;
    }
    #endregion
    
    /// <summary>
    /// Comparison for Priority Queues. Determine closer tiles based on pfDistance.
    /// </summary>
    /// <param name="otherTile">The tile we're comparing.</param>
    /// <returns></returns>
    public int CompareTo(BattleMapTile otherTile)
    {
        return pfDistance - otherTile.pfDistance;
    }

    #region To Other Data Type Methods
    /// <summary>
    /// Return the tile's position as a string.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return string.Format("(T{0})", position.ToString());
    }

    /// <summary>
    /// Return the tile's position as a Vector3Int
    /// </summary>
    /// <returns></returns>
    public Vector3Int ToVector3Int()
    {
        return new Vector3Int(position.x, position.y, 0);
    }
    #endregion

    /// <summary>
    /// Grab the actor on a tile if there's one.
    /// </summary>
    /// <returns></returns>
    public Actor GetActorOnTile()
    {
        return map.GetActorOnTile(position);
    }

    /// <summary>
    /// Determines whether the given actor can walk on this tile.
    /// </summary>
    /// <param name="actor">The actor considering this tile.</param>
    /// <returns></returns>
    public bool IsWalkable(Actor actor)
    {
        if(actor.moveType == MoveType.Ground && terrain.movementAllowed == MovementAllowed.AllMovement)
        {
            return true;
        }
        else if (actor.moveType == MoveType.Flying && terrain.movementAllowed <= MovementAllowed.FlyingOnly)
        {
            return true;
        }
        return false;
    }
}
