﻿using UnityEngine;
using System.Collections;

public class EventHandler
{
    public GameManager gm;

    public EventHandler(GameManager gm)
    {
        this.gm = gm;
    }

    public virtual void HandleButtonInput(InputData data)
    {

    }
    public virtual void HandleMouseInput(InputMouseData data)
    {

    }
}
