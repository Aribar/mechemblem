﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionTargetingHandler : EventHandler
{
    public PlayerActionTargetingHandler(GameManager gm) : base(gm)
    {
    }

    public override void HandleButtonInput(InputData data)
    {
        Vector2Int position = Vector2Int.FloorToInt(gm.mc.cursor.transform.position);
        switch (data.button)
        {
            //Directional Input
            case "Up":
                //Change target selection.
                gm.mc.combatInfoPanel.IncrementSelection(false);
                break;
            case "Down":
                //Change target selection.
                gm.mc.combatInfoPanel.IncrementSelection(true);
                break;
            case "Left":
                //Change target selection.
                gm.mc.combatInfoPanel.IncrementSelection(false);
                break;
            case "Right":
                //Change target selection.
                gm.mc.combatInfoPanel.IncrementSelection(true);
                break;
            case "Confirm":
                //ATTACK!
                gm.targetedActor = gm.mc.combatInfoPanel.GetCurrentTargetedActor();
                gm.battlesm.ChangeState<PlayerCombatSequenceState>();
                break;
            case "Cancel":
                gm.battlesm.ChangeState<PlayerActionSelectionState>();
                break;
        }
    }
}
