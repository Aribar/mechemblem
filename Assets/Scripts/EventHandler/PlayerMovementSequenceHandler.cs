﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementSequencenHandler : EventHandler
{
    public PlayerMovementSequencenHandler(GameManager gm) : base(gm)
    {
    }

    public override void HandleButtonInput(InputData data)
    {
        switch (data.button)
        {
            case "Confirm":
                gm.selectedActor.moveQuick = true;
                break;
        }
    }
}
