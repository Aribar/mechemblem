using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnitSelectionHandler : EventHandler
{
    public PlayerUnitSelectionHandler(GameManager gm) : base(gm)
    {
    }

    public override void HandleButtonInput(InputData data)
    {
        Vector2Int position = Vector2Int.FloorToInt(gm.mc.cursor.transform.position);
        switch (data.button)
        {
            //Directional Input
            case "Up":
                new MoveCursorAction(new Vector2Int(0, 1)).Perform(gm, null);
                break;
            case "Down":
                new MoveCursorAction(new Vector2Int(0, -1)).Perform(gm, null);
                break;
            case "Left":
                new MoveCursorAction(new Vector2Int(-1, 0)).Perform(gm, null);
                break;
            case "Right":
                new MoveCursorAction(new Vector2Int(1, 0)).Perform(gm, null);
                break;
            case "Confirm":
                Actor actor = gm.map.GetActorOnTile(position);
                if (actor != null && actor.faction == gm.currentFaction && actor.CanAct && actor.isAlive)
                {
                    gm.selectedActor = gm.map.GetActorOnTile(position);
                    gm.battlesm.ChangeState<PlayerMovementSelectionState>();
                }
                break;
            case "Cancel":
                break;
        }
    }
}
