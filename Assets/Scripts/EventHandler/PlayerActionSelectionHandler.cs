﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionSelectionHandler : EventHandler
{
    public PlayerActionSelectionHandler(GameManager gm) : base(gm)
    {
    }

    public override void HandleButtonInput(InputData data)
    {
        Vector2Int position = Vector2Int.FloorToInt(gm.mc.cursor.transform.position);
        switch (data.button)
        {
            //Directional Input
            case "Up":
                gm.mc.actionCategoryPanel.IncrementSelection(false);
                break;
            case "Down":
                gm.mc.actionCategoryPanel.IncrementSelection(true);
                break;
            case "Confirm":
                gm.selectedAction = gm.mc.actionCategoryPanel.GetSelection();
                switch(gm.selectedAction)
                {
                    case "Attack":
                        gm.battlesm.ChangeState<PlayerActionTargetingState>();
                        break;
                    case "Assist":
                        break;
                    case "Inventory":
                        break;
                    case "Wait":
                        new WaitAction().Perform(gm, gm.selectedActor);
                        break;
                }
                break;
            case "Cancel":
                gm.selectedActor.PositionSet(gm.selectedActor.pathMap.startTile.position, true);
                gm.battlesm.ChangeState<PlayerMovementSelectionState>();
                break;
        }
    }
}
