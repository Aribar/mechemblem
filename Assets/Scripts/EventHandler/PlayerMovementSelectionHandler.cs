﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementSelectionHandler : EventHandler
{
    public PlayerMovementSelectionHandler(GameManager gm) : base(gm)
    {
    }

    public override void HandleButtonInput(InputData data)
    {
        Vector2Int position = Vector2Int.FloorToInt(gm.mc.cursor.transform.position);
        switch (data.button)
        {
            //Directional Input
            case "Up":
                new MoveCursorAction(new Vector2Int(0, 1)).Perform(gm, null);
                break;
            case "Down":
                new MoveCursorAction(new Vector2Int(0, -1)).Perform(gm, null);
                break;
            case "Left":
                new MoveCursorAction(new Vector2Int(-1, 0)).Perform(gm, null);
                break;
            case "Right":
                new MoveCursorAction(new Vector2Int(1, 0)).Perform(gm, null);
                break;
            case "Confirm":
                new MoveActorAction(Pathfinder.SearchAStar(gm.map, gm.selectedActor, gm.selectedActor.position, position, Pathfinder.GetTileCostBase, gm.selectedActor.AddTileWithinMoveRangeStandard)).Perform(gm, gm.selectedActor);
                break;
            case "Cancel":
                gm.battlesm.ChangeState<PlayerUnitSelectionState>();
                break;
        }
    }
}
