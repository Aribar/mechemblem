using UnityEngine;

public abstract class State : MonoBehaviour
{
    public virtual void Enter()
    {
        //Debug.Log("<b>Entering State: </b>" + this.ToString());
        AddListeners();
    }

    public virtual void Exit()
    {
        //Debug.Log("Exiting State: " + this.ToString());
        RemoveListeners();
    }

    protected virtual void OnDestroy()
    {
        RemoveListeners();
    }

    protected virtual void AddListeners()
    {

    }

    protected virtual void RemoveListeners()
    {

    }
}