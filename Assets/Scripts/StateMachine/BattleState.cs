using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleState : State
{
    protected GameManager gm;

    protected virtual void Awake()
    {
        gm = GetComponent<GameManager>();
    }

    protected override void AddListeners()
    {
        //InputController.moveEvent += OnMove;
        //InputController.fireEvent += OnFire;
    }

    protected override void RemoveListeners()
    {
        //InputController.moveEvent -= OnMove;
        //InputController.fireEvent -= OnFire;
    }

    //protected virtual void OnMove(object sender, InfoEventArgs<Point> e)
    //{

    //}

    //protected virtual void OnFire(object sender, InfoEventArgs<int> e)
    //{

    //}

}
