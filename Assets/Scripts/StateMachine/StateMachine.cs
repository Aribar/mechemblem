using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    protected State _currentState;
    protected bool _inTransition;

    public virtual State CurrentState
    {
        get { return _currentState; }
        set { Transition(value); }
    }

    public virtual T GetState<T>() where T : State
    {
        T target = GetComponent<T>();
        if (target == null)
            target = gameObject.AddComponent<T>();
        return target;
    }

    public virtual void ChangeState<T>() where T : State
    {
        StartCoroutine(Sequence<T>());
    }

    private IEnumerator Sequence<T>() where T : State
    {
        bool firstRun = true;
        while (CoroutineController.NumDelayingRoutinesRunning() > 0 || firstRun)
        {
            firstRun = false;
            CoroutineController.UpdateWatchedCoroutineCheck();
            yield return null;
        }
        CurrentState = GetState<T>();
    }

    protected virtual void Transition(State value)
    {
        //If we're either already in that state or are in transition
        if (_currentState == value || _inTransition)
        {
            Debug.LogErrorFormat("STATE TRANSITION PROBLEM! We were in {0} and tried to go to {1}. _inTransition: {2}", _currentState.ToString(), value.ToString(), _inTransition);
            return;
        }
            
        _inTransition = true;

        if (_currentState != null)
        {
            _currentState.Exit();
        }

        _currentState = value;

        if (_currentState != null)
        {
            _currentState.Enter();
        }

        _inTransition = false;
    }

}
