﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementSequenceState : BattleState
{
    /// <summary>
    /// Time to move the player!
    /// </summary>
    public override void Enter()
    {
        base.Enter();
        gm.handler = new PlayerMovementSequencenHandler(gm);

        CoroutineController.StartWatchedCoroutine(gm.selectedActor.MoveAlongRoute(), true);
        gm.battlesm.ChangeState<PlayerActionSelectionState>();
    }

    /// <summary>
    /// Okay they don't need to move quick anymore.
    /// </summary>
    public override void Exit()
    {
        base.Exit();
        gm.selectedActor.moveQuick = false;
    }
}
