﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerActionSequenceState : BattleState
{
    /// <summary>
    /// Our computer has an Action! Let's Act!
    /// </summary>
    public override void Enter()
    {
        base.Enter();
        
        gm.cc.Fight(gm.computerAction.source, gm.computerAction.target);

        gm.computerAction.source.CanAct = false;
        gm.battlesm.ChangeState<ComputerRoundEvaluationState>();
    }
}
