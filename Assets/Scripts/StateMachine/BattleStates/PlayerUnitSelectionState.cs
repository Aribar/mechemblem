using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnitSelectionState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        gm.handler = new PlayerUnitSelectionHandler(gm);

        Actor actorUnderCursor = gm.map.GetActorOnTile(gm.mc.cursor.position);
        if (actorUnderCursor != null)
        {
            gm.mc.miniUnitPanel.ShowMenu(actorUnderCursor);
        }
    }

    /// <summary>
    /// Hide the Mini Unit Panel if it's up.
    /// </summary>
    public override void Exit()
    {
        base.Exit();
        gm.mc.miniUnitPanel.HideMenu();
    }
}
