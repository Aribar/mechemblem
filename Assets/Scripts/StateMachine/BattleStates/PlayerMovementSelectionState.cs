using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class PlayerMovementSelectionState : BattleState
{
    List<BattleMapTile> movementHighlights;
    List<BattleMapTile> attackHighlights;

    public override void Enter()
    {
        base.Enter();
        gm.handler = new PlayerMovementSelectionHandler(gm);

        //Find tiles and then highlight them!
        Pathfinder.GetWalkableAndAttackableTiles(gm.map, gm.selectedActor, out movementHighlights, out attackHighlights);

        gm.map.AddHighlightToTiles(attackHighlights, "Attack");
        gm.map.AddHighlightToTiles(movementHighlights, "Movement");

        Actor actorUnderCursor = gm.map.GetActorOnTile(gm.mc.cursor.position);
        if (actorUnderCursor != null)
        {
            gm.mc.miniUnitPanel.ShowMenu(actorUnderCursor);
        }
    }

    /// <summary>
    /// Hide tile highlights and the Mini Unit Panel if it's up.
    /// </summary>
    public override void Exit()
    {
        base.Exit();

        //Remove all tile highlights
        gm.map.RemoveHighlightFromTiles(movementHighlights);
        gm.map.RemoveHighlightFromTiles(attackHighlights);

        gm.mc.miniUnitPanel.HideMenu();
    }
}
