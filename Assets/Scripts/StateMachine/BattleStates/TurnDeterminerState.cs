﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnDeterminerState : BattleState
{
    Faction faction;

    public override void Enter()
    {
        base.Enter();

        //Allow all units to act
        gm.AllActorsCanAct();

        //Find the first faction that can go this round
        faction = gm.factions.Find(x => x.canAct == true);

        //Did we find a faction that can go? If so, make them our currently in-control faction!
        if (faction != null)
        {
            gm.currentFaction = faction;
        }
        //Everyone went, time to start a new round!
        else
        {
            gm.round++;
            gm.AllFactionsCanAct();
            faction = gm.factions.Find(x => x.canAct == true);
            gm.currentFaction = faction;
        }
        //StartCoroutine(ShowRoundDisplayPanel());

        gm.mc.roundDisplayPanel.ShowMenu(gm.round, gm.currentFaction.name, gm.currentFaction.actors[0].GetComponent<SpriteRenderer>().sprite);

        if (faction.playerControlled)
        {
            //Player-controlled! Start their turn.
            gm.battlesm.ChangeState<PlayerUnitSelectionState>();
        }
        else
        {
            //Computer-controlled! Start thinking.
            gm.battlesm.ChangeState<ComputerRoundEvaluationState>();
        }
    }
}