﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnitEndTurnState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        gm.handler = new EventHandler(gm);

        //If no one can act, then determine which faction goes.
        if(gm.IsTurnOver())
        {
            gm.currentFaction.canAct = false;
            gm.battlesm.ChangeState<TurnDeterminerState>();
        }
        //Someone can still act!
        else
        {
            gm.battlesm.ChangeState<PlayerUnitSelectionState>();
        }
    }
}