﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ComputerRoundEvaluationState : BattleState
{
    AIActionData bestAction = null;

    /// <summary>
    /// Evaluate which unit in the faction has the best move to make and then run with it!
    /// </summary>
    public override void Enter()
    {
        base.Enter();
        bestAction = null;

        //What tiles do each actor on the map threaten?
        gm.map.ClearTilesActorsThreaten();
        gm.map.DetermineTilesActorsThreaten();

        //For each unit, determine if they threaten anyone.
        List<AIActionData> allActions = new List<AIActionData>();
        foreach (Actor actor in gm.currentFaction.actors.Where(x => x.CanAct == true && x.isAlive))
        {
            actor.ai.DetermineThreatenedUnits();

            //For each threatened unit, determine the best tile for an action and then evaluate the attack.
            foreach (AIActionData action in actor.ai.possibleActions)
            {
                actor.ai.DetermineAttackAction(action.target);
                allActions.Add(action);
            }
        }

        //There's some possible actions! Determine the best one!
        if(allActions.Count() > 0)
        {
            bestAction = DetermineBestAction(allActions);
        }
        //Oops, no one can act. Guess we can handle only-movement actions!
        else
        {
            Actor actor = gm.currentFaction.actors.Where(x => x.CanAct == true && x.isAlive).FirstOrDefault();

            if(actor != null)
            {
                bestAction = actor.ai.DetermineMoveAction();
            }
        }

        //Now actually perform that best action!
        if (bestAction != null)
        {
            gm.computerAction = bestAction;
            gm.battlesm.ChangeState<ComputerMoveSequenceState>();
        }
        //If no one can act, I guess the turn is over!
        else
        {
            gm.currentFaction.canAct = false;
            gm.battlesm.ChangeState<TurnDeterminerState>();
        }
    }

    /// <summary>
    /// Given a list of actions for every unit, what's the best move to use?
    /// First we want only moves that deal the most damage.
    /// Then... I guess those that give the best ratio... Hm.
    /// </summary>
    /// <param name="actions">All the actions we're considering.</param>
    /// <returns></returns>
    AIActionData DetermineBestAction(List<AIActionData> actions)
    {
        List<AIActionData> actionsToRemove = new List<AIActionData>();

        //What's the best result, and remove other results?
        float maxOffense = actions.Max(x => x.combatResult.offensiveResult);
        foreach (AIActionData action in actions)
        {
            if(action.combatResult.offensiveResult < maxOffense)
            {
                actionsToRemove.Add(action);
            }
        }
        foreach (AIActionData action in actionsToRemove)
        {
            actions.Remove(action);
        }
        actionsToRemove.Clear();

        //What's the best damage ratio? Right now this is arbitrarily offenseResult * 2 - defense Result.
        float maxratio = actions.Max(x => x.CalculateRatio());
        foreach (AIActionData action in actions)
        {
            if (action.CalculateRatio() < maxratio)
            {
                actionsToRemove.Add(action);
            }
        }
        foreach (AIActionData action in actionsToRemove)
        {
            actions.Remove(action);
        }

        return actions.First();
    }

}
