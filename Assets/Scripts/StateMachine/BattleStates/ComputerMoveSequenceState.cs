﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerMoveSequenceState : BattleState
{
    /// <summary>
    /// Time to move the computer based on our given action!
    /// </summary>
    public override void Enter()
    {
        base.Enter();

        //Figure out where we're moving then move there!
        if (gm.computerAction.optimalTile != null)
        {
            gm.computerAction.source.pathMap = Pathfinder.SearchAStar(
                gm.map,
                gm.computerAction.source,
                gm.computerAction.source.position,
                gm.computerAction.optimalTile.position,
                Pathfinder.GetTileCostBase,
                gm.computerAction.source.AddTileWithinMoveRangeStandard);

            CoroutineController.StartWatchedCoroutine(gm.computerAction.source.MoveAlongRoute(), true);
        }

        //If there's still an Action to do after the move then act!
        if (gm.computerAction.combatResult != null)
        {
            gm.battlesm.ChangeState<ComputerActionSequenceState>();
        }
        //Otherwise, this unit's turn is over!
        else
        {
            gm.computerAction.source.CanAct = false;
            gm.battlesm.ChangeState<ComputerRoundEvaluationState>();
        }
    }
}
