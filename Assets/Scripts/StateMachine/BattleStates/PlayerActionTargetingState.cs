﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerActionTargetingState : BattleState
{
    List<BattleMapTile> attackHighlights;

    public override void Enter()
    {
        base.Enter();

        gm.handler = new PlayerActionTargetingHandler(gm);

        //Find tiles and highlight them!
        attackHighlights = Pathfinder.GetAttackableTiles(gm.map, gm.selectedActor, true);
        gm.map.AddHighlightToTiles(attackHighlights, "Attack");

        //Get targeted actors
        Actor potentialTarget;
        foreach(BattleMapTile tile in attackHighlights)
        {
            potentialTarget = gm.map.GetActorOnTile(tile.position);
            if (potentialTarget != null && potentialTarget.faction != gm.selectedActor.faction)
            {
                gm.mc.combatInfoPanel.targetedActors.Add(potentialTarget);
            }
        }

        //Display the combat info menu
        gm.mc.combatInfoPanel.ShowMenu(gm.selectedActor);
    }

    public override void Exit()
    {
        base.Exit();

        //Hide menu.
        gm.mc.combatInfoPanel.HideMenu();

        //Remove targeted actors.
        gm.mc.combatInfoPanel.targetedActors.Clear();

        //Remove highlights.
        gm.map.RemoveHighlightFromTiles(attackHighlights);
    }
}