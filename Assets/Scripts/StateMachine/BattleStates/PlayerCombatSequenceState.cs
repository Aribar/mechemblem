﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerCombatSequenceState : BattleState
{
    /// <summary>
    /// Have our selected actor fight the targeted actor!
    /// </summary>
    public override void Enter()
    {
        base.Enter();
        gm.handler = new EventHandler(gm);

        gm.cc.Fight(gm.selectedActor, gm.targetedActor);
        
        gm.selectedActor.CanAct = false;
        gm.battlesm.ChangeState<PlayerUnitEndTurnState>();
    }
}