﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerActionSelectionState : BattleState
{
    List<BattleMapTile> attackHighlights;

    /// <summary>
    /// Let's show the area the user can effect and list actions they can perform.
    /// </summary>
    public override void Enter()
    {
        base.Enter();
        gm.handler = new PlayerActionSelectionHandler(gm);

        //Find tiles and highlight them!
        attackHighlights = Pathfinder.GetAttackableTiles(gm.map, gm.selectedActor, true);
        gm.map.AddHighlightToTiles(attackHighlights, "Attack");

        //Bring up an Action menu.
        List<string> options = new List<string>();
        if(EnableAttack())
        {
            options.Add("Attack");
        }
        options.Add("Wait");

        gm.mc.actionCategoryPanel.ShowMenu(options);
    }

    /// <summary>
    /// Hide the menu and remove the highlights we made.
    /// </summary>
    public override void Exit()
    {
        base.Exit();

        //Turn off the menu
        gm.mc.actionCategoryPanel.HideMenu();

        //Remove highlights.
        gm.map.RemoveHighlightFromTiles(attackHighlights);
    }

    /// <summary>
    /// If we have even a single opponent we can target then enable the Attack command.
    /// </summary>
    /// <returns></returns>
    bool EnableAttack()
    {
        Actor actor;
        //Make a list of every enemy we can target.
        foreach (BattleMapTile tile in attackHighlights)
        {
            actor = gm.map.GetActorOnTile(tile.position);
            if (actor != null && actor.faction != gm.selectedActor.faction)
            {
                return true;
            }
        }
        return false;
    }
}
