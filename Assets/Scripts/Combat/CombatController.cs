﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatController : MonoBehaviour
{
    [SerializeField]
    private GameManager gm;

    public void Fight(Actor attacker, Actor target)
    {
        List<AttackResult> results = new List<AttackResult>();
        results.Add(Attack(attacker, target));
        if(results[0].result != HitResult.Kill)
        {
            results.Add(Attack(target, attacker));
        }

        //Create Health Panels
        gm.mc.healthPanelContainer.ShowMenuEmpty();
        gm.mc.healthPanelContainer.AddPanel(attacker);
        gm.mc.healthPanelContainer.AddPanel(target);

        //Do animation stuff for each attack!
        CoroutineController.StartWatchedCoroutine(AnimateAttack(results), true);

        //yield return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="attacks">The results of the attacks that just happened.</param>
    /// <returns></returns>
    IEnumerator AnimateAttack(List<AttackResult> attacks)
    {
        foreach (AttackResult attack in attacks)
        {
            if(attack.result != HitResult.DoesNotReach)
            {
                //Move to the "attack position" and wait a moment.
                yield return attack.attacker.MoveToAttack(attack.target);

                //Update our health panels with the results of the attack.
                gm.mc.healthPanelContainer.UpdatePanelCurrentHealth(attack.target, Mathf.Max(new int[] { 0, attack.target.HpCurrent - attack.damage }));

                //TODO: Pop up text for damage/miss.
                string text;
                Color color;
                Vector2 position = RectTransformUtility.WorldToScreenPoint(Camera.main, attack.target.transform.position + new Vector3(0.5f, .9f, 0f));
                if (attack.result == HitResult.Hits || attack.result == HitResult.Kill)
                {
                    text = attack.damage.ToString();
                    color = Color.red;
                }
                else //Miss!
                {
                    text = "MISS";
                    color = Color.white;
                }
                gm.mc.textPopup.Show(text, color, position);
                yield return LerpUtility.MoveUI(gm.mc.textPopup.gameObject, gm.mc.textPopup.gameObject.GetComponent<RectTransform>().anchoredPosition + new Vector2(0f, 50f), .5f, LerpUtility.SpikeEaseOutQuadratic);

                //Move back from "attack position".
                yield return attack.attacker.MoveFromAttack();
            }
            //Pause a little before the next attack.
            gm.mc.textPopup.HideMenu();
            yield return new WaitForSeconds(.25f);
        }

        //Update actor data based on results
        foreach (AttackResult result in attacks)
        {
            result.target.HpCurrent -= result.damage;
        }

        //Wait a little bit at the end then hide panels.
        yield return new WaitForSeconds(.25f);
        gm.mc.healthPanelContainer.HideMenu();

        //If anyone died, let's remove them here?
        foreach (AttackResult result in attacks)
        {
            if(!result.target.isAlive)
            {
                result.target.gameObject.SetActive(false);
            }
        }
    }

    AttackResult Attack(Actor attacker, Actor target)
    {
        AttackResult result = new AttackResult();
        result.attacker = attacker;
        result.target = target;
        int hit = Random.Range(1, 101);

        //Can the attack reach the target?
        if (attacker.GetCurrentWeapon().CanTargetTile(attacker.position, target.position))
        {
            //Did the attack hit?
            if (hit <= attacker.GetCurrentWeapon().hit)
            {
                result.result = HitResult.Hits;
                int damage = attacker.GetCurrentWeapon().damage;
                result.damage = damage;

                //Debug.Log(string.Format("{0} attacks {1} with {2} and hits ({3}) for {4} damage!",
                //    attacker.name, target.name, attacker.GetCurrentWeapon().name, hit, damage));

                //Does this kill the target?
                if (damage >= target.HpCurrent)
                {
                    result.result = HitResult.Kill;
                    //Debug.Log(string.Format("{0} is dead!", target.ToString()));
                }
            }
            else
            {
                result.result = HitResult.Miss;
                //Debug.Log(string.Format("{0} attacks {1} with {2} and misses ({3})...",
                //    attacker.name, target.name, attacker.GetCurrentWeapon().name, hit));
            }
        }
        //Can't reach target!
        else
        {
            result.result = HitResult.DoesNotReach;
        }
        return result;
    }
}
