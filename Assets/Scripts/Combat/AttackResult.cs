﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackResult
{
    public Actor attacker;
    public Actor target;
    public HitResult result;
    public int damage = 0;

    public AttackResult() { }
}
