﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatCalculator
{
    ///// <summary>
    ///// Fight!
    ///// </summary>
    ///// <param name="attacker">The attacking actor.</param>
    ///// <param name="defender">The targeted actor.</param>
    //public static List<AttackResult> ExecuteCombat(Actor attacker, Actor defender)
    //{
    //    List<AttackResult> results = new List<AttackResult>();
    //    results.Add(Attack(attacker, defender));

    //    //Defender is alive for counter
    //    if (defender.isAlive)
    //    {
    //        results.Add(Attack(defender, attacker));
    //    }

    //    return results;
    //}

    /// <summary>
    /// Returns a prediction of how combat will go.
    /// </summary>
    /// <param name="attacker">The attacking actor.</param>
    /// <param name="defender">The targeted actor.</param>
    /// <returns></returns>
    public static CombatPrediction PredictCombat(AIActionData action)
    {
        Actor attacker = action.source;
        Actor defender = action.target;
        CombatPrediction result = new CombatPrediction();

        float myDamage = CalculateDamageAsPercentage(attacker, defender);
        float myAccuracy = CalculateAccuracyAsPercentage(attacker, defender);
        float chanceToKillBeforeCounter = 0f;
        float targetDamage = 0f;
        float targetAccuracy = 0f;

        //If the defender can counter, get their info.
        if(defender.GetCurrentWeapon().CanTargetTile(defender.position, action.optimalTile.position))
        {
            targetDamage = CalculateDamageAsPercentage(defender, attacker);
            targetAccuracy = CalculateAccuracyAsPercentage(defender, attacker);

            if (myDamage == 1)
            {
                chanceToKillBeforeCounter = myDamage * myAccuracy;
            }
        }
        //Calculate offensive result
        result.offensiveResult = myDamage * myAccuracy;
        result.defensiveResult = 0 + targetDamage * targetAccuracy * (1 - chanceToKillBeforeCounter);

        return result;
    }

    ///// <summary>
    ///// See if the source's weapon can attack, determine if it hits, and deal damage.
    ///// </summary>
    ///// <param name="source">The attacking actor.</param>
    ///// <param name="target">The targeted actor.</param>
    //static AttackResult Attack(Actor source, Actor target)
    //{
    //    AttackResult result = new AttackResult();
    //    result.attacker = source;
    //    result.target = target;
    //    int hit = Random.Range(1, 101);

    //    //Can the attack target the square?
    //    if(source.GetCurrentWeapon().CanTargetTile(source.position, target.position))
    //    {
    //        //Attack hits.
    //        if(hit <= source.GetCurrentWeapon().hit)
    //        {
    //            //result.attackHits = true;
    //            int damage = source.GetCurrentWeapon().damage;
    //            result.damage = damage;
    //            target.HpCurrent -= damage;

    //            Debug.Log(string.Format("{0} attacks {1} with {2} and hits ({3}) for {4} damage!",
    //                source.name, target.name, source.GetCurrentWeapon().name, hit, damage));

    //            //Defender is dead.
    //            if (!target.isAlive)
    //            {
    //                //result.attackKills = true;
    //                Debug.Log(string.Format("{0} is dead!", target.ToString()));
    //            }
    //        }
    //        //Attack misses
    //        else
    //        {
    //            Debug.Log(string.Format("{0} attacks {1} with {2} and misses ({3})...",
    //                source.name, target.name, source.GetCurrentWeapon().name, hit));
    //        }
    //    }
    //    return result;
        
    //}

    #region Calculate Damage/Accuracy
    /// <summary>
    /// Calculates an attack's accuracy on a scale of 0-1, with 0 being 0% accuracy and 1 being 100% accuracy.
    /// </summary>
    /// <param name="attacker">The attacking actor.</param>
    /// <param name="target">The targeted actor.</param>
    /// <returns></returns>
    public static float CalculateAccuracyAsPercentage(Actor attacker, Actor target)
    {
        return Mathf.Clamp01((float) attacker.GetCurrentWeapon().hit / 100);
    }

    /// <summary>
    /// Calculates an attack's damage on a scale of 0-1, with 0 being 0 damage and 1 being the target's current health.
    /// </summary>
    /// <param name="attacker">The attacking actor.</param>
    /// <param name="target">The targeted actor.</param>
    /// <returns></returns>
    public static float CalculateDamageAsPercentage(Actor attacker, Actor target)
    {
        return Mathf.Clamp01((float) attacker.GetCurrentWeapon().damage / target.HpCurrent);
    }
    #endregion
}