using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public float speed = 20.0f;
    private Vector3 intendedPosition;
    private Camera mainCamera;

    private GameObject trackedEntity;
    
    void Awake()
    {
        mainCamera = GetComponentInChildren<Camera>();
        intendedPosition = transform.position;

        if (GameObject.Find("Cursor"))
            trackedEntity = GameObject.Find("Cursor");
    }

    private void LateUpdate()
    {
        if (trackedEntity == null)
        {
            if (GameObject.Find("Cursor"))
                trackedEntity = GameObject.Find("Cursor");
        }
        if (trackedEntity &&
            (intendedPosition.x != trackedEntity.transform.localPosition.x ||
            intendedPosition.y != trackedEntity.transform.localPosition.y
            ))
        {
            intendedPosition.x = trackedEntity.transform.localPosition.x;
            intendedPosition.y = trackedEntity.transform.localPosition.y;
        }
        MoveCamera();
    }

    private void MoveCamera()
    {
        Vector3 newPosition = Vector3.Lerp(transform.position, intendedPosition, speed * Time.fixedDeltaTime);
        transform.position = newPosition;
        //yield return StartCoroutine(MoveLerp(newPosition));
    }
}
