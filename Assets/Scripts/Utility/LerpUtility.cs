﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class LerpUtility
{
    #region Easing Methods
    //For all of these "t" is the Current Time and "d" is the Duration of the lerp.
    public static float EaseInQuadratic(float t, float d)
    {
        return t * t;
    }

    public static float EaseOutQuadratic(float t, float d)
    {
        t /= d;
        return -1 * t * (t - 2);
    }

    //Untested.
    public static float Flip(float x)
    {
        return 1 - x;
    }

    public static float Linear(float t, float d)
    {
        return t / d;
    }

    public static float SpikeEaseOutQuadratic(float t, float d)
    {
        return EaseOutQuadratic(t, d / 2);
    }
    #endregion

    #region Movement Methods
    /// <summary>
    /// Move an actor from its current location to a set destination over a length of time.
    /// </summary>
    /// <param name="actor">The actor we're moving</param>
    /// <param name="destination">Our final destination</param>
    /// <param name="duration">How long the move will take</param>
    /// <param name="easingFunction">The easing function controlling the animation</param>
    /// <returns></returns>
    public static IEnumerator MoveActor(Actor actor, Vector2 destination, float duration, Func<float, float, float> easingFunction)
    {
        float currentTime = 0f;
        Vector2 origin = actor.gameObject.transform.position;

        while (currentTime < duration)
        {
            actor.gameObject.transform.position = Vector2.Lerp(origin, destination, easingFunction(currentTime, duration));
            //Debug.Log(string.Format("Current Time: {0} || Progress {1} || Ease {2} || Position {3} ", currentTime, currentTime / duration, easingFunction(currentTime, duration), go.transform.position.ToString()));

            if(actor.moveQuick)
            {
                currentTime += Time.deltaTime * 2;
            }
            else
            {
                currentTime += Time.deltaTime;
            }
            yield return null;
        }
        currentTime = duration;
        actor.gameObject.transform.position = Vector2.Lerp(origin, destination, easingFunction(currentTime, duration));
    }

    /// <summary>
    /// Move a UI element from its current location to a set destination over a length of time.
    /// </summary>
    /// <param name="go">The element we're moving</param>
    /// <param name="destination">Our final destination</param>
    /// <param name="duration">How long the move will take</param>
    /// <param name="easingFunction">The easing function controlling the animation</param>
    public static IEnumerator MoveUI(GameObject go, Vector2 destination, float duration, Func<float, float, float> easingFunction)
    {
        float currentTime = 0f;
        Vector2 origin = go.GetComponent<RectTransform>().anchoredPosition;

        while (currentTime < duration)
        {
            go.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(origin, destination, easingFunction(currentTime, duration));
            //Debug.Log(string.Format("Current Time / Duration: {0} / {1} || Progress {2} || Ease {3} || Position {4} ", currentTime, duration, currentTime / duration, easingFunction(currentTime, duration), go.transform.position.ToString()));
            currentTime += Time.deltaTime;
            yield return null;
        }
        currentTime = duration;
        go.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(origin, destination, easingFunction(currentTime, duration));
    }
    #endregion
}
