﻿using System;

public enum ActionType
{
    MoveOnly,
    MoveAndAttack
}

public enum HitResult
{
    Kill,
    Hits,
    Miss,
    DoesNotReach
}

[Serializable]
public enum MovementAllowed
{
    AllMovement,
    FlyingOnly,
    NoneAllowed
}

[Serializable]
public enum MoveType
{
    Ground,
    Flying
}