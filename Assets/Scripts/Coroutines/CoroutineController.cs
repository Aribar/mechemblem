﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CoroutineController : MonoBehaviour
{
    private HashSet<WatchedCoroutine> watchedCoroutines = new HashSet<WatchedCoroutine>();
    private static CoroutineController instance;

    /// <summary>
    /// Singleton!
    /// </summary>
    public void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    /// <summary>
    /// Fancy thing to make a coroutine into a WatchedCoroutine that we can actually manage!
    /// </summary>
    /// <param name="coroutine">The coroutine we're watching</param>
    /// <param name="shouldDelayGame">Should we keep the game from going forward while this coroutine executes?</param>
    /// <returns></returns>
    public static WatchedCoroutine StartWatchedCoroutine(IEnumerator coroutine, bool shouldDelayGame)
    {
        var newWatcher = new WatchedCoroutine();

        if(shouldDelayGame)
        {
            if (instance.watchedCoroutines == null)
            {
                instance.watchedCoroutines = new HashSet<WatchedCoroutine>();
            }
            instance.watchedCoroutines.Add(newWatcher);
        }

        instance.StartCoroutine(newWatcher.StartCoroutine(coroutine));

        return newWatcher;
    }

    /// <summary>
    /// How many WatchedCoroutines do we have?
    /// </summary>
    /// <returns></returns>
    public static int NumDelayingRoutinesRunning()
    {
        return instance.watchedCoroutines.Count;
    }

    /// <summary>
    /// If any of our WatchedCoroutines are finished then remove them from our hashset.
    /// </summary>
    public static void UpdateWatchedCoroutineCheck()
    {
        instance.watchedCoroutines?.RemoveWhere(wc => wc.IsFinished());
    }

}
