﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WatchedCoroutine
{
    private bool isCoroutineFinished;
    private IEnumerator myCoroutine;

    //When this is true, we throw on the brakes and abandon ship.
    private bool shouldStopThisCoroutine;

    //When this is true, we do not update, but may yet update later.
    private bool isPaused;

    public bool IsFinished()
    {
        return isCoroutineFinished;
    }

    public bool IsPaused() { return isPaused; }

    public void Pause() { isPaused = true; }
    public void Unpause() { isPaused = false; }

    public string GetCoroutineName()
    {
        return myCoroutine?.ToString() ?? "[null]";
    }

    //the loop that runs the coroutine must be 
    //itself a coroutine! I herd you like etc etcetc
    public IEnumerator StartCoroutine(IEnumerator routine)
    {
        myCoroutine = routine;
        while (!shouldStopThisCoroutine && myCoroutine.MoveNext())
        {
            //return whatever the coroutine does, which can be something like
            //wait x seconds, wait for next frame, or just a yield return null
            yield return myCoroutine.Current;

            //if we choose to pause the coroutine, just hang out here. 
            while (isPaused)
            {
                if (shouldStopThisCoroutine)
                {
                    StopCoroutine();
                    yield break;
                }
                yield return null;
            }
        }

        StopCoroutine();
    }

    public void StopCoroutine()
    {
        //we're gonna set a stop flag that our StartCoroutine loop will then
        //catch and we will abandon ship.
        shouldStopThisCoroutine = true;

        //We are also finished
        isCoroutineFinished = true;
    }
}
