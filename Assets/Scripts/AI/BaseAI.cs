﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BaseAI
{
    private readonly Actor actor;
    
    public List<AIActionData> possibleActions = new List<AIActionData>();

    public BaseAI(Actor actor)
    {
        this.actor = actor;
    }

    /// <summary>
    /// Determine what opponents are targetable within a unit's entire movement/threat range.
    /// Each possible target will be added to the BaseAI's possibleActions for later consideration.
    /// </summary>
    public void DetermineThreatenedUnits()
    {
        possibleActions.Clear();

        List<BattleMapTile> targetableTiles = Pathfinder.GetAttackableTiles(actor.map, actor, false);
        foreach (BattleMapTile tile in targetableTiles)
        {
            Actor possibleTarget = tile.GetActorOnTile();
            //Below will need to be changed if we don't assume all factions are hostile to each other!!!
            if (possibleTarget && actor.faction != possibleTarget.faction && possibleTarget.isAlive)
            {
                possibleActions.Add(new AIActionData(actor, possibleTarget));
            }
        }
    }

    /// <summary>
    /// Look at every possible tile we can attack the target, apply filtering, and evaluate how good the attack is.
    /// </summary>
    /// <param name="target">The actor we're targeting.</param>
    public void DetermineAttackAction(Actor target)
    {
        //Find every tile that can act on the target.
        Dictionary<BattleMapTile, int> possibleTiles = Pathfinder.GetTilesForAttackingTarget(actor, target);

        //Unimplemented: Would this result in a Win/Draw/Loss?
        //Unimplemented: What's the best damage ratio?
        //Unimplemented: Is this a defensive tile?

        //What tile has the lowest threateningActors?
        RemoveTilesBasedOnThreatLevel(possibleTiles);

        //What tile has the lowest movement required?
        RemoveTilesBasedOnThreatLevel(possibleTiles);

        //What tile has the lowest... id or something?
        possibleActions.Find(x => x.target == target).optimalTile = possibleTiles.First().Key;

        //EvaluateAttack(target);
        EvaluateAttack(possibleActions.Find(x => x.target == target));
    }

    /// <summary>
    /// Predict how well an attack's going to go against a target and record it.
    /// </summary>
    /// <param name="action">The attack action we're evaluating.</param>
    public void EvaluateAttack(AIActionData action)
    {
        action.combatResult = CombatCalculator.PredictCombat(action);
        action.actionType = ActionType.MoveAndAttack;
    }

    /// <summary>
    /// Assuming the Actor has MoveRange, check every living actor in a different faction and evaluate how good moving towards them is.
    /// </summary>
    /// <returns></returns>
    public AIActionData DetermineMoveAction()
    {
        AIActionData result = new AIActionData(actor, null);
        Dictionary<Actor, int> possibleTargets = new Dictionary<Actor, int>();

        //We only care about finding a move action if the unit can move!
        if(actor.moveRange > 0)
        {
            //Get the distance away for every living actor in a different faction.
            foreach (Actor target in actor.map.actors.Where(x => x.faction != actor.faction && x.isAlive))
            {
                int distance = Pathfinder.GetMovementCostBetweenTwoPoints(actor.position, target.position, actor);
                int damage = actor.GetCurrentWeapon().damage - 2 * Mathf.CeilToInt(distance / actor.moveRange);
                possibleTargets.Add(target, damage);
            }

            //Assuming we have at least one target, what is the most damage? We'll ultimately just take the first one here.
            if (possibleTargets.Count > 0)
            {
                RemoveTilesBasedOnDamage(possibleTargets);

                result.target = possibleTargets.First().Key;
                result.optimalTile = EvaluateMove(result.target);
            }
        }
        
        return result;
    }

    /// <summary>
    /// Determine how well each of our moves is at reaching the target.
    /// </summary>
    /// <param name="target">The actor we're trying to reach.</param>
    /// <returns></returns>
    BattleMapTile EvaluateMove(Actor target)
    {
        //Find every tile that can act on the target.
        Dictionary<BattleMapTile, int> possibleTiles = Pathfinder.GetDestinationTilesWithCost(actor);

        //Filter based on distance to target
        RemoveTilesBasedOnDistanceToTarget(possibleTiles, target);

        //Filter based on threat of tile
        RemoveTilesBasedOnThreatLevel(possibleTiles);

        //Filter based on movement required
        RemoveTilesBasedOnMovementRequired(possibleTiles);

        //Return first tile
        return possibleTiles.Keys.First();
    }

    /// <summary>
    /// Find the most damage possible and filter out any targets with less potential damage than that.
    /// </summary>
    /// <param name="targets">The targets we're examining and filtering.</param>
    void RemoveTilesBasedOnDamage(Dictionary<Actor, int> targets)
    {
        int maxDamage = targets.Max(x => x.Value);
        List<Actor> targetsToRemove = new List<Actor>();

        foreach (KeyValuePair<Actor, int> kvp in targets)
        {
            if (kvp.Value < maxDamage)
            {
                targetsToRemove.Add(kvp.Key);
            }
        }
        foreach (Actor target in targetsToRemove)
        {
            targets.Remove(target);
        }
    }

    /// <summary>
    /// Find the closest tile to the target and filter out any tiles with more distance than that.
    /// </summary>
    /// <param name="tiles">The tiles we're examining and filtering.</param>
    /// <param name="target">The actor we're moving towards.</param>
    void RemoveTilesBasedOnDistanceToTarget(Dictionary<BattleMapTile, int> tiles, Actor target)
    {
        int minDistance = tiles.Min(x => Pathfinder.GetMovementCostBetweenTwoPoints(x.Key.position, target.position, actor));
        List <BattleMapTile> tilesToRemove = new List<BattleMapTile>();

        //Find all the tiles we want to remove.
        foreach (KeyValuePair<BattleMapTile, int> kvp in tiles)
        {
            if (Pathfinder.GetMovementCostBetweenTwoPoints(kvp.Key.position, target.position, actor) > minDistance)
            {
                tilesToRemove.Add(kvp.Key);
            }
        }
        //Remove those tiles!
        foreach (BattleMapTile tile in tilesToRemove)
        {
            tiles.Remove(tile);
        }
    }

    /// <summary>
    /// Find the least movement required and filter out any tiles with more movement than that.
    /// </summary>
    /// <param name="tiles">The tiles we're examining and filtering.</param>
    void RemoveTilesBasedOnMovementRequired(Dictionary<BattleMapTile, int> tiles)
    {
        int minMovement = tiles.Min(x => x.Value);
        List<BattleMapTile> tilesToRemove = new List<BattleMapTile>();

        //Find all the tiles we want to remove.
        foreach (KeyValuePair<BattleMapTile, int> kvp in tiles)
        {
            if (kvp.Value > minMovement)
            {
                tilesToRemove.Add(kvp.Key);
            }
        }
        //Remove those tiles!
        foreach (BattleMapTile tile in tilesToRemove)
        {
            tiles.Remove(tile);
        }
    }

    /// <summary>
    /// Find the least threatened tile and filter out any tiles more threatened than that.
    /// </summary>
    /// <param name="tiles">The tiles we're examining and filtering.</param>
    void RemoveTilesBasedOnThreatLevel(Dictionary<BattleMapTile, int> tiles)
    {
        int minThreatened = tiles.Min(x => x.Key.targetingActors.Count);
        List<BattleMapTile> tilesToRemove = new List<BattleMapTile>();

        //Find all the tiles we want to remove.
        foreach (BattleMapTile tile in tiles.Keys)
        {
            if (tile.targetingActors.Count > minThreatened)
            {
                tilesToRemove.Add(tile);
            }
        }
        //Remove those tiles!
        foreach (BattleMapTile tile in tilesToRemove)
        {
            tiles.Remove(tile);
        }
    }
}
