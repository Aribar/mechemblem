﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AIActionData
{
    public Actor source;
    public Actor target;
    public BattleMapTile optimalTile;
    public CombatPrediction combatResult;
    public ActionType actionType;

    public AIActionData(Actor source, Actor target)
    {
        this.source = source;
        this.target = target;
    }

    /// <summary>
    /// Figure out the ratio of offense vs defense to determine how good the attack is.
    /// </summary>
    /// <returns></returns>
    public float CalculateRatio()
    {
        return combatResult.offensiveResult * 2 - combatResult.defensiveResult;
    }

    public override string ToString()
    {
        return string.Format("AIActionData: {0}({1}) attack {2}({3}): O{4} / D{5} / Total {6}", source, source.GetCurrentWeapon().name, target, target.GetCurrentWeapon().name, combatResult.offensiveResult, combatResult.defensiveResult, CalculateRatio());
    }
}
