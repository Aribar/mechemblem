﻿using UnityEngine;
using System.Collections;

public class InputData
{
    public string button;

    public InputData(string button)
    {
        this.button = button;
    }
}