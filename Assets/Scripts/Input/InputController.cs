﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.InputSystem;


public class InputController : MonoBehaviour
{
    //public static event EventHandler<IAction> ActionEvent;
    //public static event EventHandler<IAction> MoveEvent;

    //Take two at this stuff...
    public static event EventHandler<InputData> InputButtonEvent;
    public static event EventHandler<InputMouseData> InputMouseEvent;

    GameManager gm;

    private void Awake()
    {
        gameObject.GetComponent<PlayerInput>().onActionTriggered += HandleInput;
        gm = gameObject.GetComponent<GameManager>();
    }

    private void HandleInput(InputAction.CallbackContext ctx)
    {
        //Debug.Log(string.Format("Input: {0}", ctx.action.name));
        if (ctx.started)
        {
            //if (ctx.action.activeControl.device == InputSystem.GetDevice("Mouse"))
            //    InputMouseEvent?.Invoke(this, new InputMouseData(ctx.action.name, Mouse.current.position.ReadValue()));
            ////Debug.Log(string.Format("Input: {0}", ctx.action.activeControl.device));
            //else
                InputButtonEvent?.Invoke(this, new InputData(ctx.action.name));
        }
    }
}