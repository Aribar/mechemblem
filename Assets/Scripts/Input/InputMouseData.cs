﻿using UnityEngine;
using System.Collections;

public class InputMouseData : InputData
{
    public Vector2 position;

    public InputMouseData(string button, Vector2 position) : base(button)
    {
        this.button = button;
        this.position = position;
    }
}