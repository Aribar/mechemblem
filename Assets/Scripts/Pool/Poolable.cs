﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//http://theliquidfire.com/2015/07/06/object-pooling/
public class Poolable : MonoBehaviour
{
    public string key;
    public bool isPooled;
}