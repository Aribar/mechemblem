using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public EventHandler handler;
    //Battle Map data
    public BattleMap map;

    //Unit stuff?
    public Actor selectedActor;
    public string selectedAction;
    public Actor targetedActor;
    public AIActionData computerAction;

    public List<Faction> factions = new List<Faction>();
    public Faction currentFaction;

    //State Machines???
    public StateMachine battlesm;

    //Controllers
    public GUIController mc;
    public CombatController cc;

    //Factories
    WeaponFactory wf = new WeaponFactory();

    public int round = 1;

    // Start is called before the first frame update
    void Start()
    {

        //Helpful thing for editor
        #if UNITY_EDITOR
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 45;
        #endif
        //Grab Controllers.
        mc = gameObject.GetComponent<GUIController>();
        cc = gameObject.GetComponent<CombatController>();

        //Create and render the map
        map = new BattleMap(this, "Test");
        map.RenderMap();

        //Creating factions
        factions.Add(new Faction(true, "Player"));
        factions.Add(new Faction(false, "Enemy"));

        //Making weapons?
        wf.PopulateFactory();

        //Populate the map with units.
        SpawnUnit(factions[0], new Vector2Int(3, 5), "Blade");
        SpawnUnit(factions[0], new Vector2Int(3, 6), "AssaultRifle");
        SpawnUnit(factions[0], new Vector2Int(3, 7), "Pistol");

        SpawnUnit(factions[1], new Vector2Int(10, 5), "Blade");
        SpawnUnit(factions[1], new Vector2Int(10, 6), "AssaultRifle");
        SpawnUnit(factions[1], new Vector2Int(10, 7), "Pistol");

        //Add a cursor?
        mc.cursor = Entity.CreateEntity("Cursor", map, new Vector2Int(2, 4), Resources.Load<Sprite>("Sprites/Selector"));
        mc.cursor.gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "EntityUI";

        //Start listening for input
        handler = new EventHandler(this);
        AddListeners();

        //And go to a state!
        battlesm = gameObject.AddComponent<StateMachine>();
        battlesm.ChangeState<TurnDeterminerState>();
    }

    void OnDestroy()
    {
        RemoveListeners();
    }


    #region Add/Remove Listeners
    /// <summary>
    /// Add listeners to the Input Controller.
    /// </summary>
    void AddListeners()
    {
        InputController.InputButtonEvent += OnButtonInput;
        InputController.InputMouseEvent += OnMouseInput;
    }

    /// <summary>
    /// Remove listeners to the Input Controller.
    /// </summary>
    void RemoveListeners()
    {
        InputController.InputButtonEvent -= OnButtonInput;
        InputController.InputMouseEvent -= OnMouseInput;
    }
    #endregion

    #region Input Stuff
    /// <summary>
    /// Handle button input from the listeners.
    /// </summary>
    /// <param name="sender">The Input Controller.</param>
    /// <param name="data">The button that was pressed.</param>
    private void OnButtonInput(object sender, InputData data)
    {
        //Debug.Log(string.Format("Input: {0}", data.button));
        handler.HandleButtonInput(data);
    }

    /// <summary>
    /// Handle mouse input from the listeners.
    /// </summary>
    /// <param name="sender">The Input Controller.</param>
    /// <param name="data">Info on what kind of mouse stuff happened.</param>
    private void OnMouseInput(object sender, InputMouseData data)
    {
        handler.HandleMouseInput(data);
    }
    #endregion


    #region Turn/Round/Action Management
    /// <summary>
    /// Check all actors in the current faction. If at least one is alive and can act, turn's not over.
    /// Player-only...
    /// </summary>
    /// <returns></returns>
    public bool IsTurnOver()
    {
        foreach(Actor actor in currentFaction.actors)
        {
            if (actor.CanAct && actor.isAlive)
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Sets all actors' CanAct to true.
    /// </summary>
    public void AllActorsCanAct()
    {
        foreach (Actor actor in map.actors)
        {
            actor.CanAct = true;
        }
    }

    /// <summary>
    /// Sets all factions' CanAct to true.
    /// </summary>
    public void AllFactionsCanAct()
    {
        foreach (Faction faction in factions)
        {
            faction.canAct = true;
        }
    }
    #endregion


    //Just temporary "spawn some doods" stuff.
    void SpawnUnit(Faction faction, Vector2Int position, string weapon)
    {
        Actor result = null;
        if (faction.name == "Player")
        {
            result = Actor.CreateActor(faction.name + " Unit " + position.ToString(), position, Resources.Load<Sprite>("Sprites/HappyBoi"), 3, map);
        }
        else
        {
            result = Actor.CreateActor(faction.name + " Unit " + position.ToString(), position, Resources.Load<Sprite>("Sprites/BadBoi"), 3, map);
        }

        //Actor result = Actor.CreateActor(faction.name + " Unit " + position.ToString(), position, Resources.LoadAll<Sprite>("Sprites/Player")[5], 3, map);

        //Add the new actor to their Faction
        ChangeActorFaction(result, faction);

        //Give them a weapon
        Weapon newWeapon = wf.weapons.Find(x => x.name == weapon);
        result.weapons.Add(newWeapon);

        //Add them to the map
        map.actors.Add(result);

        //return result;
    }

    /// <summary>
    /// Update an actor to a new faction.
    /// </summary>
    /// <param name="actor">The actor we're modifying.</param>
    /// <param name="newFaction">The faction we're adding them to.</param>
    public void ChangeActorFaction(Actor actor, Faction newFaction)
    {
        //Remove from existing faction list if there's already one
        if(actor.faction != null)
        {
            factions.Find(x => x == actor.faction).actors.Remove(actor);
        }

        //Add to new faction list
        factions.Find(x => x == newFaction).actors.Add(actor);

        //Update faction!
        actor.faction = newFaction;
    }
}
