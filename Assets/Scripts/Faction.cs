﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Faction
{
    public bool playerControlled;
    public bool canAct;
    public string name;

    public List<Actor> actors = new List<Actor>();

    public Faction(bool playerControlled, string name)
    {
        this.playerControlled = playerControlled;
        this.name = name;
        canAct = true;
    }
}
